linux:
	fpc -vewn -Tlinux -gl fprl.pp -olambdarogue

win32:
	fpc -vewn -Twin32 -gl fprl.pp -olambdarogue.exe

debug-linux:
	fpc -vewn -Tlinux -ghl -Criot fprl.pp -olambdarogue

debug-win32:
	fpc -vewn -Twin32 -ghl -Criot fprl.pp -olambdarogue.exe

clean:
	rm -rf *.o *.ppu
