{
     Copyright (C) 2006-2008 by Mario Donick    
     mario.donick@gmail.com    
                                                                            
     This program is free software; you can redistribute it and/or modify   
     it under the terms of the GNU General Public License as published by   
     the Free Software Foundation; either version 2 of the License, or      
     (at your option) any later version.                                    
                                                                            
     This program is distributed in the hope that it will be useful,        
     but WITHOUT ANY WARRANTY; without even the implied warranty of         
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
     GNU General Public License for more details.                           
                                                                            
     You should have received a copy of the GNU General Public License      
     along with this program; if not, write to the                          
     Free Software Foundation, Inc.,                                        
     59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              
}

unit Output;

interface

uses
    SDL, SDL_image, Crt, Video, BaseOutput, SysUtils, RandomArea, Player, Items,
    Chants, CollectData, ExternSFX;

var
    MessageLogFile: Textfile;



procedure BlendTitleSurface (blend: boolean);
procedure BlendMagic (n: integer);
procedure Meteor;


procedure ShowMonsterSpell (n: integer);
procedure ShowExplosion(x, y: integer);
procedure ShowTrapEffect;


procedure InventoryMenu(n: integer);
procedure SpellbookMenu(n: integer);
procedure ShopMenu(n, ShopID: integer);
procedure StealMenu(n, ShopID: integer);

procedure TooltipGodSelectM(x, y: integer);
procedure TooltipProfSelectM(x, y: integer);
procedure TooltipProfSelectF(x, y: integer);
procedure TooltipSkillTrain(x, y: integer);

implementation





procedure BlendTitleSurface (blend: boolean);
var
    i: Integer;
    BlitRect: SDL_RECT;
begin
    if UseSDL=true then
    begin

        BlitRect.w:=800;
        BlitRect.h:=0;
        BlitRect.x:=0+HiResOffsetX;
        BlitRect.y:=0+HiResOffsetY;

        if blend=true then
        begin
            for i:=0 to 40 do
            begin
                SDL_SETALPHA(title, SDL_SRCALPHA, i);
                SDL_BLITSURFACE(title, nil, screen, @BlitRect);
                SDL_UPDATERECT(screen,0,0,0,0);
                delay(10);
            end;
        end
        else
        begin
            SDL_BLITSURFACE(title, nil, screen, @BlitRect);
            SDL_UPDATERECT(screen,0,0,0,0);
        end;
    end;
end;


procedure BlendMagic(n: integer);
var
    i: Integer;
    strSub: String;
begin
    if UseSDL=true then
    begin
        if UseHiRes=false then
            //strSub:='/800x600/'
            strSub:='/800x480/'
        else
            strSub:='/1024x768/';

        case n of
            1:
                LoadImage_Back(PathData+'graphics/fx'+strSub+'sfx-fire.png');
            2:
                LoadImage_Back(PathData+'graphics/fx'+strSub+'sfx-ice.png');
            3:
                LoadImage_Back(PathData+'graphics/fx'+strSub+'sfx-water.png');
            4:
                LoadImage_Back(PathData+'graphics/fx'+strSub+'sfx-poison.png');
            5:
                LoadImage_Back(PathData+'graphics/fx'+strSub+'sfx-bless.png');
            6:
                LoadImage_Back(PathData+'graphics/fx'+strSub+'sfx-heal.png');
            7:
                LoadImage_Back(PathData+'graphics/fx'+strSub+'sfx-pp.png');
            8:
                LoadImage_Back(PathData+'graphics/fx'+strSub+'sfx-curse.png');
            9:
                LoadImage_Back(PathData+'graphics/fx'+strSub+'sfx-barrier.png');
            10:
                LoadImage_Back(PathData+'graphics/fx'+strSub+'sfx-confusion.png');
            11:
                LoadImage_Back(PathData+'graphics/fx'+strSub+'sfx-blind.png');
            12:
                LoadImage_Back(PathData+'graphics/fx'+strSub+'sfx-para.png');
            13:
                LoadImage_Back(PathData+'graphics/fx'+strSub+'sfx-force.png');
            14:
                LoadImage_Back(PathData+'graphics/fx'+strSub+'sfx-healaura.png');
            15:
                LoadImage_Back(PathData+'graphics/fx'+strSub+'sfx-fireatt.png');
            16:
                LoadImage_Back(PathData+'graphics/fx'+strSub+'sfx-iceatt.png');
            17:
                LoadImage_Back(PathData+'graphics/fx'+strSub+'sfx-wateratt.png');
            18:
                LoadImage_Back(PathData+'graphics/fx'+strSub+'sfx-meteor.png');
            19:
                LoadImage_Back(PathData+'graphics/fx'+strSub+'sfx-lichtbringer.png');
            20:
                LoadImage_Back(PathData+'graphics/fx'+strSub+'sfx-spiderweb.png');
        end;

        for i:=0 to 5 do
        begin
            SDL_SETALPHA(backgroundGraph, SDL_SRCALPHA, i);
            SDL_BLITSURFACE(backgroundGraph, nil, screen, nil);
            SDL_UPDATERECT(screen,0,0,0,0);
            delay(30);
        end;
        if UseHiRes=false then
            LoadImage_Back(PathData+'graphics/bg-800.png')
        else
            LoadImage_Back(PathData+'graphics/bg-1024.png');
        ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
    end;
end;


// Meteor
procedure Meteor;
begin
    PlaySFX('magic-time.mp3');
    BlendMagic(11);
    BlendMagic(18);
end;


procedure TooltipGodSelectM(x, y: integer);
var
    Tooltip: String;
begin
    Tooltip:='-';

    if (x>=120) and (y>=60) and (x<=220) and (y<=185) then
        Tooltip:='Aphrodite: Move +2'; // aphrodite
    if (x>=280) and (y>=65) and (x<=405) and (y<=190) then
        Tooltip:='Hermes: Lance +2'; // hermes
    if (x>=445) and (y>=90) and (x<=555) and (y<=225) then
        Tooltip:='Apoll: Burgle +2'; // apoll
    if (x>=560) and (y>=210) and (x<=665) and (y<=335) then
        Tooltip:='Believers in Dyonysos need less food than other characters.'; // dionysos
    if (x>=570) and (y>=345) and (x<=705) and (y<=490) then
        Tooltip:='Ares: Sword +3 / Axe +3'; // ares

    if Tooltip<>'-' then
    begin
        TextXY(1, 27, '                                                                            ');
        TextXY(1, 27, Tooltip);
        SDL_UPDATERECT(screen,0, 500,800,100);
    end;
end;


procedure TooltipProfSelectM(x, y: integer);
var
    Tooltip: String;
begin
    Tooltip:='-';

    if (x>=30) and (y>=150) and (x<=155) and (y<=310) then
        Tooltip:='Constructor: Tool +6 / clears contaminated ground'; // constructor
    if (x>=175) and (y>=160) and (x<=305) and (y<=330) then
        Tooltip:='Enchanter: PP +30 / View +3 / Chant +6'; // enchanter
    if (x>=325) and (y>=170) and (x<=440) and (y<=315) then
        Tooltip:='Thief: Burgle +6'; // thief
    if (x>=465) and (y>=175) and (x<=605) and (y<=305) then
        Tooltip:='Ranger: Fire-arm +6 / View +3'; // ranger
    if (x>=645) and (y>=170) and (x<=780) and (y<=290) then
        Tooltip:='Soldier: Fight +5 / Move +3'; // soldier

    if Tooltip<>'-' then
    begin
        TextXY(1, 27, '                                                                            ');
        TextXY(1, 27, Tooltip);
        SDL_UPDATERECT(screen,0, 500,800,100);
    end;
end;

procedure TooltipProfSelectF(x, y: integer);
var
    Tooltip: String;
begin
    Tooltip:='-';

    if (x>=0) and (y>=145) and (x<=160) and (y<=320) then
        Tooltip:='Constructor: Tool +6 / clears contaminated ground'; // constructor
    if (x>=161) and (y>=85) and (x<=320) and (y<=320) then
        Tooltip:='Enchanter: PP +30 / View +3 / Chant +6'; // enchanter
    if (x>=340) and (y>=105) and (x<=430) and (y<=255) then
        Tooltip:='Thief: Burgle +6'; // thief
    if (x>=431) and (y>=60) and (x<=674) and (y<=300) then
        Tooltip:='Ranger: Fire-arm +6 / View +3'; // ranger
    if (x>=675) and (y>=180) and (x<=800) and (y<=350) then
        Tooltip:='Soldier: Fight +5 / Move +3'; // soldier

    if Tooltip<>'-' then
    begin
        TextXY(1, 27, '                                                                            ');
        TextXY(1, 27, Tooltip);
        SDL_UPDATERECT(screen,0, 500,800,100);
    end;
end;

// tooltips for skill train screen
procedure TooltipSkillTrain(x, y: integer);
var
    Tooltip: String;
begin

    Tooltip:='-';

    // are we in skills area?
    if (x>=228) and (x<=475) then
    begin

        // abilities
        if (y>=199) and (y<=246) then
        begin
            if (x>=228) and (x<=275) then
                Tooltip:='Ability: Fight. Influences your general skills in combat.';  // Fight
            if (x>=278) and (x<=325) then
                Tooltip:='Ability: View. Determines if long-range or magical attacks hit or miss.';  // View
            if (x>=328) and (x<=375) then
                Tooltip:='Ability: Chant. Determines the success of casting a chant.';  // Chant
            if (x>=378) and (x<=425) then
                Tooltip:='Ability: Move. Influences your ability to evade attacks.';  // Move
            if (x>=428) and (x<=475) then
                Tooltip:='Ability: Burgle. Allows you to break doors and to avoid traps.';  // Burgle
        end;


        // skills
        if (y>=259) and (y<=306) then
        begin
            if (x>=228) and (x<=275) then
                Tooltip:='Skill: Sword. Determines the efficiency in using swords.';  // Sword
            if (x>=278) and (x<=325) then
                Tooltip:='Skill: Axe. Determines the efficiency in using axes.';  // Axe
            if (x>=328) and (x<=375) then
                Tooltip:='Skill: Lance. Determines the efficiency in using lances.';  // Lance
            if (x>=378) and (x<=425) then
                Tooltip:='Skill: Firearm. Determines the efficiency in using bows.';  // Firearm
            if (x>=428) and (x<=475) then
                Tooltip:='Skill: Tool. Determines the efficiency in using spades.';  // Tool
        end;


        // talents
        if (y>=319) and (y<=366) then
        begin
            if (x>=228) and (x<=275) then
                Tooltip:='Talent: Humility. Affects your relation to your deity.';  // Humility
            if (x>=278) and (x<=325) then
                Tooltip:='Talent: Trade. Gives you advantages when buying or selling.';  // Trade
        end;
    end;

    if Tooltip<>'-' then
    begin
        TextXY(1, 27, '                                                                            ');
        TextXY(1, 27, Tooltip);
        SDL_UPDATERECT(screen,0, 500,800,100);
    end;
end;

// shop menu (for mouse interface)
procedure ShopMenu(n, ShopID: integer);
var
    i: Integer;
begin
    ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);

    for i:=8 to 15 do
        TextXY(12, i, chr(211)+'                                      '+chr(212));

    TextXY(12, 8,  chr(205)+'                                      '+chr(206));
    TextXY(12, 15, chr(207)+'                                      '+chr(208));

    for i:=13 to 50 do
    begin
        TextXY(i, 8, chr(209));
        TextXY(i, 15, chr(210));
    end;
              
    TextXY(14, 9, Thing[MyShop[ShopID].Inventory[n].intType].strRealName);
    TextXY(16, 11, 'Buy item');
    TextXY(16, 12, 'Look at item');
    TextXY(14, 14, 'Cancel');

    ShowMessage('Select an option from the menu.', false);
end;


// steal menu (for mouse interface)
procedure StealMenu(n, ShopID: integer);
var
    i: Integer;
begin
    ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);

    for i:=8 to 15 do
        TextXY(12, i, chr(211)+'                                      '+chr(212));

    TextXY(12, 8,  chr(205)+'                                      '+chr(206));
    TextXY(12, 15, chr(207)+'                                      '+chr(208));

    for i:=13 to 50 do
    begin
        TextXY(i, 8, chr(209));
        TextXY(i, 15, chr(210));
    end;

    TextXY(14, 9, Thing[MyShop[ShopID].Inventory[n].intType].strName);
    TextXY(16, 11, 'Steal item');
    TextXY(16, 12, 'Look at item');
    TextXY(14, 14, 'Cancel');

    ShowMessage('Select an option from the menu.', false);
end;

// spellbook menu (for mouse interface)
procedure SpellbookMenu(n: integer);
var
    i: Integer;
begin
    ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);

    for i:=8 to 15 do
        TextXY(12, i, chr(211)+'                                      '+chr(212));

    TextXY(12, 8,  chr(205)+'                                      '+chr(206));
    TextXY(12, 15, chr(207)+'                                      '+chr(208));

    for i:=13 to 50 do
    begin
        TextXY(i, 8, chr(209));
        TextXY(i, 15, chr(210));
    end;
              
    TextXY(14, 9, Spell[spellbook[n].intType].strName);
    TextXY(16, 11, 'Chant song');
    TextXY(16, 12, 'Info about song');
    TextXY(14, 14, 'Cancel');

    ShowMessage('Select an option from the menu.', false);
end;

// inventory menu (for mouse interface)
procedure InventoryMenu(n: integer);
var
    i: Integer;
begin
    ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);

    for i:=8 to 19 do
        TextXY(12, i, chr(211)+'                                         '+chr(212));

    TextXY(12, 8,  chr(205)+'                                         '+chr(206));
    TextXY(12, 19, chr(207)+'                                         '+chr(208));

    for i:=13 to 53 do
    begin
        TextXY(i, 8, chr(209));
        TextXY(i, 19, chr(210));
    end;

    TextXY(14, 9, Thing[Inventory[n].intType].chLetter + ' '+Thing[Inventory[n].intType].strName);

    if (Thing[Inventory[n].intType].blWield=true) or (Thing[Inventory[n].intType].blWear=true) or (Thing[Inventory[n].intType].blHat=true) or (Thing[Inventory[n].intType].blRing=true) then
        if (ThePlayer.intWeapon<>Inventory[n].intType) and (ThePlayer.intArmour<>Inventory[n].intType) and (ThePlayer.intHat<>Inventory[n].intType) and (ThePlayer.intRingLeft<>Inventory[n].intType) and (ThePlayer.intRingRight<>Inventory[n].intType) then
            TextXY(16, 11, 'Equip ' + Thing[Inventory[n].intType].strName);

    if (Thing[Inventory[n].intType].blWield=true) or (Thing[Inventory[n].intType].blWear=true) or (Thing[Inventory[n].intType].blHat=true) or (Thing[Inventory[n].intType].blRing=true) then
        if (ThePlayer.intWeapon=Inventory[n].intType) or (ThePlayer.intArmour=Inventory[n].intType) or (ThePlayer.intHat=Inventory[n].intType) or (ThePlayer.intRingLeft=Inventory[n].intType) or (ThePlayer.intRingRight=Inventory[n].intType) then
            TextXY(16, 12, 'Remove ' + Thing[Inventory[n].intType].strName);

    if (DngLvl[ThePlayer.intX, ThePlayer.intY].intBuilding>0) and (DngLvl[ThePlayer.intX, ThePlayer.intY].intBuilding<>8) then
        TextXY(16, 13, 'Sell to trader for '+IntToStr(CollectSellPrice(Inventory[n].intType))+' Credits');
    if (DngLvl[ThePlayer.intX, ThePlayer.intY].intBuilding=8) then
        TextXY(16, 13, 'Disassemble in factory');
    if (DngLvl[ThePlayer.intX, ThePlayer.intY].intFloorType=15) then
        TextXY(16, 13, 'Sacrifice item to '+ThePlayer.strReli);
    if (DngLvl[ThePlayer.intX, ThePlayer.intY].intBuilding=0) and (DngLvl[ThePlayer.intX, ThePlayer.intY].intFloorType<>15) then
        TextXY(16, 13, 'Drop item on Floor');

    TextXY(16, 14, 'Look at '+Thing[Inventory[n].intType].strName);

    if (Thing[Inventory[n].intType].strTextfile<>'-') or (Thing[Inventory[n].intType].chLetter=chr(161)) then
        TextXY(16, 15, 'Study '+Thing[Inventory[n].intType].strName);

    if (Thing[Inventory[n].intType].blEat=true) or (Thing[Inventory[n].intType].blDrink=true) then
        TextXY(16, 16, 'Consume (Eat or Drink)');

    TextXY(14, 18, 'Cancel');

    ShowMessage('Select an option from the menu.', false);
end;




// show the spell of a monster
procedure ShowMonsterSpell (n: integer);
var
    ch1, ch2: char;
    intY: integer;
begin
    case n of
        // fire
        1:
        begin
            ch1:=chr(130);
            ch2:=chr(131);
        end;

        // ice
        2:
        begin
            ch1:=chr(132);
            ch2:=chr(133);
        end;

        // water
        3:
        begin
            ch1:=chr(195);
            ch2:=chr(133);
        end;
    end;

    if n<4 then
    begin

        if UseSDL=true then
            intY := ThePlayer.intBY+1
        else
            intY := ThePlayer.intBY;

        AnyCharXY (ThePlayer.intBX, intY, ch1, 0);
        if UseSDL=true then
            SDL_UPDATERECT(screen,0,0,0,0)
        else
            UpdateScreen(true);
        delay(trunc(random(80))+50);

        AnyCharXY (ThePlayer.intBX, intY, ch2, 0);
        if UseSDL=true then
            SDL_UPDATERECT(screen,0,0,0,0)
        else
            UpdateScreen(true);
        delay(trunc(random(80))+50);

        AnyCharXY (ThePlayer.intBX, intY, chr(179), 0);
        if UseSDL=true then
            SDL_UPDATERECT(screen,0,0,0,0)
        else
            UpdateScreen(true);
    end;
end;

// show explosion
procedure ShowExplosion(x, y: integer);
var
   i: integer;
begin
    for i:=1 to 6 do
    begin
        AnyCharXY(x, y, chr(130), 0);
        if UseSDL=true then
            SDL_UPDATERECT(screen,0,0,0,0)
        else
            UpdateScreen(true);

        delay(10);

        AnyCharXY(x, y, chr(131), 0);
        if UseSDL=true then
            SDL_UPDATERECT(screen,0,0,0,0)
        else
            UpdateScreen(true);
    end;
end;


// show trap
procedure ShowTrapEffect;
begin
    if UseSDL=true then
        ShowExplosion(ThePlayer.intBX, ThePlayer.intBY+1)
    else
        ShowExplosion(ThePlayer.intBX, ThePlayer.intBY);
end;

end.
