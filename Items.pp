{
     Copyright (C) 2006-2008 by Mario Donick    
     mario.donick@gmail.com
                                                                            
     This program is free software; you can redistribute it and/or modify   
     it under the terms of the GNU General Public License as published by   
     the Free Software Foundation; either version 2 of the License, or      
     (at your option) any later version.                                    
                                                                            
     This program is distributed in the hope that it will be useful,        
     but WITHOUT ANY WARRANTY; without even the implied warranty of         
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
     GNU General Public License for more details.                           
                                                                            
     You should have received a copy of the GNU General Public License      
     along with this program; if not, write to the                          
     Free Software Foundation, Inc.,                                        
     59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              
}

unit Items;


interface

uses
	RandomArea, Player, Chants, SysUtils;

const
	MaxItem = 500;
	MaxUnItem = 100;
	MaxShops = 20;

	CONST_MINIMUMITEMSTRENGTH = 35;		// minimum strength level of items that don't have their own


type Item = record
	strName, strRealName, strGenericName, strTextfile: string;
	blWield, blWear, blDrink, blEat, blThrow, blShoot: boolean;
	intPrice, intAmount, intSpellID: integer;
	blCursed, blRing, blHat, blShoes, blExtra, blBarricade, blTrap, blIdentified: boolean;
	intWP, intAP, intGP, intLight: integer;
	intEffect, intRange, intSP, intRuneSet: integer;
	strDescri, strEfText, strLearnChant: string;
	chLetter: char;
	intMinLvl, intNeedsAmmu, intIsAmmu, intStrength, intProf, intSex: integer;
	blUnique, blRare, blShopOnly, blSacrifice, blTwoHands: boolean;
	intCharLvl: longint;
end;

type ResPack = record
	longWood: longint;
	longMetal: longint;
	longStone: longint;
	longLeather: longint;
	longPlastics: longint;
	longPaper: longint;
end;


type InvEntry = record
	intType: integer;
	longNumber: integer;
end;

type Shop = record
	Inventory: array[1..16] of InvEntry;
	intType: integer;
end;

var
	Thing: array[1..MaxItem] of Item;
	Inventory: array[1..16] of InvEntry;	// default inventory
	MyShop: array[1..MaxShops] of Shop;	// shops
	Shops: array[1..MaxShops] of String;
	chBuilding: array[1..MaxShops] of char;
	Storage: ResPack;
	ItemCount: longint;
	UnItem: array[1..MaxUnItem] of integer;

function CheckEffect (e: Integer): boolean;
procedure ItemInit;
function RuneSet (Rune1, Rune2, Rune3, Rune4, Rune5: string; RemoveRunes: boolean): boolean;
function PlayerHasKey(): integer;
function PlayerHasItem(strName: string): integer;
function PlayerHasUnknownItem(strName: string): integer;
function CheckNeededItems(strItems: string; RemoveItems: boolean): boolean;
function CheckNeededResources(strResources: string): boolean;
procedure ReduceResources(strResources: string);
function ReturnItemByName(s: String): Integer;
function ReturnRandomItem: Integer;
function ReturnRareItem: Integer;
function ReturnUniqueItem: Integer;


implementation

// this function returns true, if a player has equipped an item with the given effect
// it also returns true if the temporary resistance with that effectno. is > 0
function CheckEffect (e: Integer): boolean;
begin
	CheckEffect:=false;

	if ThePlayer.intWeapon>0 then
		if Thing[ThePlayer.intWeapon].intEffect=e then CheckEffect:=true;

	if ThePlayer.intArmour>0 then
		if Thing[ThePlayer.intArmour].intEffect=e then CheckEffect:=true;

	if ThePlayer.intHat>0 then
		if Thing[ThePlayer.intHat].intEffect=e then CheckEffect:=true;

	if ThePlayer.intRingLeft>0 then
		if Thing[ThePlayer.intRingLeft].intEffect=e then CheckEffect:=true;

	if ThePlayer.intRingRight>0 then
		if Thing[ThePlayer.intRingRight].intEffect=e then CheckEffect:=true;

	if ThePlayer.intExtra>0 then
		if Thing[ThePlayer.intExtra].intEffect=e then CheckEffect:=true;

	if ThePlayer.intFeet>0 then
		if Thing[ThePlayer.intFeet].intEffect=e then CheckEffect:=true;

    if ThePlayer.intTempResist[e]>0 then
        CheckEffect:=true;
end;

// split a items-string into its components and return true if all needed items exist
function CheckNeededItems(strItems: string; RemoveItems: boolean): boolean;
var
	position: integer;
	dummy, Item1, Item2, Item3, Item4, Item5: string;
begin
	CheckNeededItems:=false;
	position:=0;

	// item 1
	dummy:='';
	repeat
		inc(position);
		if strItems[position]<>',' then
			dummy:=dummy+strItems[position];
	until strItems[position]=',';
	Item1:=dummy;
// 	writeln(dummy);

	// item 2
	dummy:='';
	repeat
		inc(position);
		if strItems[position]<>',' then
			dummy:=dummy+strItems[position];
	until strItems[position]=',';
	Item2:=dummy;
// 	writeln(dummy);

	// item 3
	dummy:='';
	repeat
		inc(position);
		if strItems[position]<>',' then
			dummy:=dummy+strItems[position];
	until strItems[position]=',';
	Item3:=dummy;
// 	writeln(dummy);

	// item 4
	dummy:='';
	repeat
		inc(position);
		if strItems[position]<>',' then
			dummy:=dummy+strItems[position];
	until strItems[position]=',';
	Item4:=dummy;
// 	writeln(dummy);

	// item 5
	dummy:='';
	repeat
		inc(position);
		if position<=length(strItems) then
			dummy:=dummy+strItems[position];
	until position=length(strItems);
	Item5:=dummy;
// 	writeln(dummy);

	CheckNeededItems:=RuneSet (Item1, Item2, Item3, Item4, Item5, RemoveItems);
end;

// split a resource-string into its components and return true if all needed resources exist
function CheckNeededResources(strResources: string): boolean;
var
	longNeedWood, longNeedMetal, longNeedStone, longNeedLeather: longint;
	longNeedPlastics, longNeedPaper: longint;
	position: integer;
	dummy: string;
begin
	// strResources contains the number of needed resources (e.g. for quests or for
	// building something in the form wood,metal,stone,leather,plastics,paper). These
	// values have to be split into numbers and then checked against the storage room
	// of the character.

	CheckNeededResources:=false;
	position:=0;

	// wood
	dummy:='';
	repeat
		inc(position);
		if strResources[position]<>',' then
			dummy:=dummy+strResources[position];
	until strResources[position]=',';
	val(dummy,longNeedWood);
	writeln(dummy);

	// metal
	dummy:='';
	repeat
		inc(position);
		if strResources[position]<>',' then
			dummy:=dummy+strResources[position];
	until strResources[position]=',';
	val(dummy,longNeedMetal);
	writeln(dummy);

	// stone
	dummy:='';
	repeat
		inc(position);
		if strResources[position]<>',' then
			dummy:=dummy+strResources[position];
	until strResources[position]=',';
	val(dummy,longNeedStone);
	writeln(dummy);

	// leather
	dummy:='';
	repeat
		inc(position);
		if strResources[position]<>',' then
			dummy:=dummy+strResources[position];
	until strResources[position]=',';
	val(dummy,longNeedLeather);
	writeln(dummy);

	// plastics
	dummy:='';
	repeat
		inc(position);
		if strResources[position]<>',' then
			dummy:=dummy+strResources[position];
	until strResources[position]=',';
	val(dummy,longNeedPlastics);
	writeln(dummy);

	// paper
	dummy:='';
	repeat
		inc(position);
		if position<=length(strResources) then
			dummy:=dummy+strResources[position];
	until position=length(strResources);
	val(dummy,longNeedPaper);
	writeln(dummy);

	// now check if all needed resources are in storage
	if (Storage.longWood>=longNeedWood) and (Storage.longMetal>=longNeedMetal) and (Storage.longStone>=longNeedStone) and (Storage.longLeather>=longNeedLeather) and (Storage.longPlastics>=longNeedPlastics) and (Storage.longPaper>=longNeedPaper) then
		CheckNeededResources:=true;

end;


// reduce player's resources by the number given in a resource-string
procedure ReduceResources(strResources: string);
var
	longNeedWood, longNeedMetal, longNeedStone, longNeedLeather: longint;
	longNeedPlastics, longNeedPaper: longint;
	position: integer;
	dummy: string;
begin
	position:=0;

	// wood
	dummy:='';
	repeat
		inc(position);
		if strResources[position]<>',' then
			dummy:=dummy+strResources[position];
	until strResources[position]=',';
	val(dummy,longNeedWood);

	// metal
	dummy:='';
	repeat
		inc(position);
		if strResources[position]<>',' then
			dummy:=dummy+strResources[position];
	until strResources[position]=',';
	val(dummy,longNeedMetal);

	// stone
	dummy:='';
	repeat
		inc(position);
		if strResources[position]<>',' then
			dummy:=dummy+strResources[position];
	until strResources[position]=',';
	val(dummy,longNeedStone);

	// leather
	dummy:='';
	repeat
		inc(position);
		if strResources[position]<>',' then
			dummy:=dummy+strResources[position];
	until strResources[position]=',';
	val(dummy,longNeedLeather);

	// plastics
	dummy:='';
	repeat
		inc(position);
		if strResources[position]<>',' then
			dummy:=dummy+strResources[position];
	until strResources[position]=',';
	val(dummy,longNeedPlastics);

	// paper
	dummy:='';
	repeat
		inc(position);
		if strResources[position]<>',' then
			dummy:=dummy+strResources[position];
	until position=length(strResources);
	val(dummy,longNeedPaper);

	dec(Storage.longWood,longNeedWood);
	dec(Storage.longMetal,longNeedMetal);
	dec(Storage.longStone,longNeedStone);
	dec(Storage.longLeather,longNeedLeather);
	dec(Storage.longPlastics,longNeedPlastics);
	dec(Storage.longPaper,longNeedPaper);
end;


// check if the player has a dungeon key or a picklock; returns -1 if not available
function PlayerHasKey(): integer;
begin
	PlayerHasKey:=-1;

	// Dungeon key for single use
	PlayerHasKey := PlayerHasItem('Dungeon Key');

	// Picklock for multiple uses
	if (PlayerHasItem('Picklock')>0) and (ThePlayer.intProf=3) then
		PlayerHasKey:=22;

	if (ThePlayer.intBurgle>10) and (random(500)>450-ThePlayer.intBurgle) then
		PlayerHasKey:=22;
end;


// check if the player has a specific item
function PlayerHasItem(strName: string): integer;
var
	i: integer;
begin
	PlayerHasItem:=-1;
	for i:=1 to 16 do
		if Inventory[i].intType>0 then
			if Thing[Inventory[i].intType].strName=strName then
				PlayerHasItem:=i;
end;

// check if the player has a specific, but not yet identified item
function PlayerHasUnknownItem(strName: string): integer;
var
	i: integer;
begin
	PlayerHasUnknownItem:=-1;
	for i:=1 to 16 do
		if Inventory[i].intType>0 then
			if Thing[Inventory[i].intType].strRealName=strName then
				PlayerHasUnknownItem:=i;
end;

// return item id by item name
function ReturnItemByName(s: String): Integer;
var
	i: Integer;
begin
	i:=0;
	repeat
		inc(i);
	until (i=ItemCount) or (thing[i].strRealName=s);
	ReturnItemByName:=i;
end;

// returns a random rare item
function ReturnRareItem: Integer;
var
	i: Integer;
begin
	repeat
		i:=trunc(1+random(ItemCount));
	until (thing[i].intMinLvl <= DungeonLevel) and (thing[i].blRare=true) ;
	ReturnRareItem:=i;
end;

// returns a random unique items
function ReturnUniqueItem: Integer;
var
	i, j: Integer;
begin
    j := 0;
    i := -1;
	repeat
		i:=trunc(1+random(ItemCount));
        inc(j);
	until (j=2000) or ((thing[i].intMinLvl = DungeonLevel) and (thing[i].blUnique=true));

    if i=-1 then
        i:=1;

    // if Player is evil and has killed Eris, drop Book of Stars
    if ThePlayer.blEvil=true then
        if DungeonLevel=20 then
            i:=ReturnItemByName('Book of Stars');

	ReturnUniqueItem:=i;
end;

// returs a random item
function ReturnRandomItem: Integer;
var
	i: Integer;
begin
	repeat
		i:=trunc(1+random(ItemCount));
	until (thing[i].intMinLvl <= DungeonLevel) and (thing[i].blUnique=false) and (thing[i].blRare=false) and (thing[i].blShopOnly=false) ;
	ReturnRandomItem:=i;
end;



// check for the existence of complete set items (and runes)
function RuneSet (Rune1, Rune2, Rune3, Rune4, Rune5: string; RemoveRunes: boolean): boolean;
var
	Runes, i, RunePos1, RunePos2, RunePos3, RunePos4, RunePos5: integer;
begin
	RunePos1 := 0;
	RunePos2 := 0;
	RunePos3 := 0;
	RunePos4 := 0;
	RunePos5 := 0;

	Runes:=0;
	RuneSet:=false;

	for i:=1 to 16 do
		if Inventory[i].intType>0 then
		begin
			if Thing[Inventory[i].intType].strName=Rune1 then
			begin
				inc(Runes);
				RunePos1 := i;
				Rune1:='[n/a]';
			end;

			if Thing[Inventory[i].intType].strName=Rune2 then
			begin
				inc(Runes);
				RunePos2 := i;
				Rune2:='[n/a]';
			end;

			if Thing[Inventory[i].intType].strName=Rune3 then
			begin
				inc(Runes);
				RunePos3 := i;
				Rune3:='[n/a]';
			end;

			if Thing[Inventory[i].intType].strName=Rune4 then
			begin
				inc(Runes);
				RunePos4 := i;
				Rune4:='[n/a]';
			end;

			if Thing[Inventory[i].intType].strName=Rune5 then
			begin
				inc(Runes);
				RunePos5 := i;
				Rune5:='[n/a]';
			end;
		end;

		if Runes=5 then
		begin
			RuneSet:=true;

			// remove all runes?
			if RemoveRunes=true then
			begin
				dec(Inventory[RunePos1].longNumber);
                if Inventory[RunePos1].longNumber=0 then
                    Inventory[RunePos1].intType:=0;

				dec(Inventory[RunePos2].longNumber);
                if Inventory[RunePos2].longNumber=0 then
                    Inventory[RunePos2].intType:=0;

				dec(Inventory[RunePos3].longNumber);
                if Inventory[RunePos3].longNumber=0 then
                    Inventory[RunePos3].intType:=0;

				dec(Inventory[RunePos4].longNumber);
                if Inventory[RunePos4].longNumber=0 then
                    Inventory[RunePos4].intType:=0;

				dec(Inventory[RunePos5].longNumber);
                if Inventory[RunePos5].longNumber=0 then
                    Inventory[RunePos5].intType:=0;
			end;
		end;
end;


procedure ItemInit;
var
	ItemFile: textfile;
	i, u: integer;
	strKey, strValue: string;
begin

	strKey := '';
	strValue := '';

	for i:=1 to MaxItem do
	begin
		with Thing[i] do
		begin
			blWear:=false;
			blWield:=false;
			blEat:=false;
			blDrink:=false;
			blThrow:=false;
			blShoot:=false;
			blUnique:=false;
			blRare:=false;
			blShopOnly:=false;
			blSacrifice:=false;
			blHat:=false;
			blRing:=false;
			blTwoHands:=false;
            blExtra:=false;
            blShoes:=false;
			blBarricade:=false;
			blTrap:=false;
			intNeedsAmmu:=0;
			intIsAmmu:=0;
			intSP:=0;
			intGP:=0;
			intWP:=0;
			intAP:=0;
			intProf:=0;
            intSex:=0;  // 0: both; 1: male; 2: female
			intAmount:=1;
			strDescri:='-';
			chLetter:='-';
			strLearnChant:='-';
			strTextfile:='-';
			intEffect:=0;
			strEfText:='';

			strName:='-';
            strGenericName:='an item';
            strRealName:='-';

			intRange:=4;
			intMinLvl:=1;
			intCharLvl:=1;
			intPrice:=1;
			intSpellID:=-1;
            blIdentified := true;
			intRuneSet:=0;
			intStrength:=CONST_MINIMUMITEMSTRENGTH;
		end;
	end;

	for i:=1 to MaxUnItem do
		UnItem[i]:=0;

	i:=0;
	u:=0;

	Assign (ItemFile, PathData+'data/items.txt');
	Reset (ItemFile);

	while eof(ItemFile)=false do
	begin
		repeat
			ReadLn(ItemFile, strKey);
		until ((strKey<>'') and (strKey[1]<>'#')) or (eof(ItemFile));

		ReadLn(ItemFile, strValue);

		if strKey='NewItem:' then inc(i);

		if i>0 then
		begin
			if strKey='NewItem:' then Thing[i].strRealName := strValue;
	
			if (strKey='Hands') and (strValue='= True') then Thing[i].blWield := true;
			if (strKey='Hands') and (strValue='= False') then Thing[i].blWield := false;
	
			if (strKey='Body') and (strValue='= True') then Thing[i].blWear := true;
			if (strKey='Body') and (strValue='= False') then Thing[i].blWear := false;

			if (strKey='Finger') and (strValue='= True') then Thing[i].blRing := true;
			if (strKey='Finger') and (strValue='= False') then Thing[i].blRing := false;

			if (strKey='Barricade') and (strValue='= True') then Thing[i].blBarricade := true;
			if (strKey='Barricade') and (strValue='= False') then Thing[i].blBarricade := false;

			if (strKey='Trap') and (strValue='= True') then Thing[i].blTrap := true;
			if (strKey='Trap') and (strValue='= False') then Thing[i].blTrap := false;

			if (strKey='Head') and (strValue='= True') then Thing[i].blHat := true;
			if (strKey='Head') and (strValue='= False') then Thing[i].blHat := false;

			if (strKey='Feet') and (strValue='= True') then Thing[i].blShoes := true;
			if (strKey='Feet') and (strValue='= False') then Thing[i].blShoes := false;

			if (strKey='Extra') and (strValue='= True') then Thing[i].blExtra := true;
			if (strKey='Extra') and (strValue='= False') then Thing[i].blExtra := false;

			if (strKey='Eat') and (strValue='= True') then Thing[i].blEat := true;
			if (strKey='Eat') and (strValue='= False') then Thing[i].blEat := false;

			if (strKey='Drink') and (strValue='= True') then Thing[i].blDrink := true;
			if (strKey='Drink') and (strValue='= False') then Thing[i].blDrink := false;

			if (strKey='Throw') and (strValue='= True') then Thing[i].blThrow := true;
			if (strKey='Throw') and (strValue='= False') then Thing[i].blThrow := false;

			if (strKey='Shoot') and (strValue='= True') then Thing[i].blShoot := true;
			if (strKey='Shoot') and (strValue='= False') then Thing[i].blShoot := false;

			if (strKey='Rare') and (strValue='= True') then Thing[i].blRare := true;
			if (strKey='Rare') and (strValue='= False') then Thing[i].blRare := false;

			if (strKey='ShopOnly') and (strValue='= True') then Thing[i].blShopOnly := true;
			if (strKey='ShopOnly') and (strValue='= False') then Thing[i].blShopOnly := false;

			if (strKey='Unique') and (strValue='= True') then
			begin
				Thing[i].blUnique := true;
				inc(u);
				UnItem[u] := i;
			end;

			if (strKey='Unique') and (strValue='= False') then Thing[i].blUnique := false;

			if (strKey='Sacrifice') and (strValue='= True') then Thing[i].blSacrifice := true;
			if (strKey='Sacrifice') and (strValue='= False') then Thing[i].blSacrifice := false;

			if (strKey='TwoHands') and (strValue='= True') then Thing[i].blTwoHands := true;
			if (strKey='TwoHands') and (strValue='= False') then Thing[i].blTwoHands := false;

			if strKey='Description:' then Thing[i].strDescri := strValue;
			if strKey='Chant:' then Thing[i].strLearnChant := strValue;

			if strKey='NeedsAmmu:' then Thing[i].intNeedsAmmu := StrToInt(strValue);
			if strKey='IsAmmu:' then Thing[i].intIsAmmu := StrToInt(strValue);
			if strKey='SpellID:' then Thing[i].intSpellID := StrToInt(strValue);

			if strKey='ItemType' then  // this defines only the icon for the item; not its function!
			begin
				if strValue='= Sword' then
                begin
                    Thing[i].chLetter := chr(156); // '/';
                    Thing[i].strGenericName := 'unknown sword';
                end;

				if strValue='= Spade' then
                begin
                    Thing[i].chLetter := chr(162); // '/';
                    Thing[i].strGenericName := 'unknown tool';
                end;

				if strValue='= Axe' then
                begin
                    Thing[i].chLetter := chr(159); // '\';
                    Thing[i].strGenericName := 'unknown axe';
                end;

				if strValue='= Lance' then
                begin
                    Thing[i].chLetter := chr(164); // '~';
                    Thing[i].strGenericName := 'unknown lance';
                end;

				if strValue='= Clothes' then
                begin
                    Thing[i].chLetter := chr(163); //'(';
                    Thing[i].strGenericName := 'unknown clothes';
                end;

				if strValue='= Armour' then
                begin
                    Thing[i].chLetter := chr(157); // '{';
                    Thing[i].strGenericName := 'unknown armour';
                end;

				if strValue='= Gun' then
                begin
                    Thing[i].chLetter := chr(158); // '}';
                    Thing[i].strGenericName := 'unknown firearm';
                end;

				if strValue='= Potion' then
                begin
                    Thing[i].chLetter := chr(155); // '!';
                    Thing[i].strGenericName := 'unknown potion';
                end;

				if strValue='= Scroll' then
                begin
                    Thing[i].chLetter := chr(161); // '?';
                    Thing[i].strGenericName := 'unknown crystal';
                end;

				if strValue='= Food' then
                begin
                    Thing[i].chLetter := chr(168); // '%';
                    Thing[i].strGenericName := 'unknown food';
                end;

				if strValue='= Torch' then
                begin
                    Thing[i].chLetter := chr(172); // '&';
                    Thing[i].strGenericName := 'unknown torch';
                end;

				if strValue='= Ring' then
                begin
                    Thing[i].chLetter := chr(166); //'=';
                    Thing[i].strGenericName := 'unknown ring';
                end;

				if strValue='= Shield' then
                begin
                    Thing[i].chLetter := chr(246); //'=';    replace with unique symbol and graphics
                    Thing[i].strGenericName := 'unknown shield';
                end;

				if strValue='= Helmet' then
                begin
                    Thing[i].chLetter := chr(165); // ')';
                    Thing[i].strGenericName := 'unknown helmet';
                end;

				if strValue='= Shoes' then
                begin
                    Thing[i].chLetter := chr(247); // ')';   replace with unique symbol and graphics
                    Thing[i].strGenericName := 'unknown shoes';
                end;

				if strValue='= Rock' then
                begin
                    Thing[i].chLetter := chr(171); //':';
                    Thing[i].strGenericName := 'unknown rock';
                end;

				if strValue='= Rune' then
                begin
                    Thing[i].chLetter := chr(169);  // '-';
                    Thing[i].strGenericName := 'unknown rune';
                end;

				if strValue='= Ammu' then
                begin
                    Thing[i].chLetter := chr(160); // '"';
                    Thing[i].strGenericName := 'unknown ammunition';
                end;

				if strValue='= Gold' then
                begin
                    Thing[i].chLetter := chr(167); //'$';
                    Thing[i].strGenericName := 'unknown gold';
                end;

				if strValue='= Key' then
                begin
                    Thing[i].chLetter := chr(191); //'o';
                    Thing[i].strGenericName := 'unknown key';
                end;

				if strValue='= Page' then
                begin
                    Thing[i].chLetter := chr(193); // '�';
                    Thing[i].strGenericName := 'unknown paper';
                end;

			end;

			if strKey='EffectText:' then Thing[i].strEfText := strValue;

			if strKey='EffectType' then
			begin
				// effects of food and potions
				if strValue='= DecHunger' then Thing[i].intEffect := 1;		// food
				if strValue='= DecPoison' then Thing[i].intEffect := 2;		// antidot
				if strValue='= FireAura' then Thing[i].intEffect := 8;		// cast FireAura
				if strValue='= IceAura' then Thing[i].intEffect := 9;		// cast IceAura
				if strValue='= DecConfusion' then Thing[i].intEffect := 12;	// decr. confusion
				if strValue='= MagicWall' then Thing[i].intEffect := 15;	// barrier
				if strValue='= IncSTR' then Thing[i].intEffect := 16;		// increase STR
				if strValue='= DecBlindness' then Thing[i].intEffect := 18;	// decrease blindness
				if strValue='= Force' then Thing[i].intEffect := 19;		// force
				if strValue='= HealAura' then Thing[i].intEffect := 20;		// heal aura
				if strValue='= Teleport' then Thing[i].intEffect := 21;		// teleport
				if strValue='= WaterAura' then Thing[i].intEffect := 22;	// cast WaterAura
				if strValue='= DecHP' then Thing[i].intEffect := 23;		// decrease HP
				if strValue='= DecPP' then Thing[i].intEffect := 24;		// decrease PP
				if strValue='= DecPara' then Thing[i].intEffect := 27;		// decr. paraliz.
				if strValue='= DecCalm' then Thing[i].intEffect := 29;		// decr. calm
				if strValue='= FreezeTime' then Thing[i].intEffect := 30;	// freeze time
				if strValue='= Uncurse' then Thing[i].intEffect := 32;		// uncurse
				if strValue='= Bless' then Thing[i].intEffect := 33;		// bless
				if strValue='= Curse' then Thing[i].intEffect := 34;		// curse
				if strValue='= Random' then Thing[i].intEffect := -1;		// <random effect>
				if strValue='= Fire' then Thing[i].intEffect := 38;		// fire spell
				if strValue='= Ice' then Thing[i].intEffect := 39;		// ice spell
				if strValue='= Water' then Thing[i].intEffect := 40;		// water spell
				if strValue='= CloneItem' then Thing[i].intEffect := 42;	// clone item
				if strValue='= IncConfusion' then Thing[i].intEffect := 44;	// incr. confusion
				if strValue='= IncPoison' then Thing[i].intEffect := 45;	// incr. poison
				if strValue='= Meteor' then Thing[i].intEffect := 46;		// summon meteor
                if strValue='= Identify' then Thing[i].intEffect := 47;     // identify item
                if strValue='= Wipe' then Thing[i].intEffect := 50;         // wipe out all monsters of current DLV

				// effects of wearables
                // (these are also valid if intTempResist[n] is greater than 0! So potions and
                //  food are possible which provide temporary resistances)
				if strValue='= IncPP' then Thing[i].intEffect := 3;		// regen. PP
				if strValue='= IncHP' then Thing[i].intEffect := 4;		// regen. HP
				if strValue='= BecomeInvisible' then Thing[i].intEffect := 5;	// invisible (also as potion!)
				if strValue='= ExtraEXP' then Thing[i].intEffect := 6;		// extra EXP
				if strValue='= ExtraGold' then Thing[i].intEffect := 7;         // extra Cr
				if strValue='= ResistIce' then Thing[i].intEffect := 14;	// resist ice spells
				if strValue='= ResistWater' then Thing[i].intEffect := 35;	// resist water spells
				if strValue='= ResistFire' then Thing[i].intEffect := 13;	// resist fire spells
				if strValue='= ResistBlindness' then Thing[i].intEffect := 17;	// resist blindness
				if strValue='= IncMove' then Thing[i].intEffect := 25;		// increase move skill
				if strValue='= ResistParalization' then Thing[i].intEffect := 26; // resist paraliz.
				if strValue='= ResistCalm' then Thing[i].intEffect := 28;	// resist calm
				if strValue='= ResistCurse' then Thing[i].intEffect := 31;	// resist curse
				if strValue='= MaxMagic' then Thing[i].intEffect := 37;		// doubles spell effect
				if strValue='= Undestroyable' then Thing[i].intEffect := 36;	// item is undestr.
				if strValue='= ResistPoison' then Thing[i].intEffect := 10;	// resist poison
				if strValue='= ResistConfusion' then Thing[i].intEffect := 11;	// resist confusion
				if strValue='= AvoidTrap' then Thing[i].intEffect := 41;     // prevent traps and trapdoors
				if strValue='= Compass' then Thing[i].intEffect := 43;		// shows a compass rose
				if strValue='= RemoveDarkness' then Thing[i].intEffect := 48;		// dark areas are enlightened
				if strValue='= RageBonus' then Thing[i].intEffect := 49;		// increases divine rage
                if strValue='= ResistDrainPP' then Thing[i].intEffect := 51;    // resist drain PP
                if strValue='= ResistDrainSTR' then Thing[i].intEffect := 52;    // resist drain PP
			end;

			if strKey='EffectRange:' then Thing[i].intRange := StrToInt(strValue);

			if strKey='Price:' then Thing[i].intPrice := StrToInt(strValue);
			if strKey='Amount:' then Thing[i].intAmount := StrToInt(strValue);

			if strKey='WP:' then Thing[i].intWP := StrToInt(strValue);	// weapon points
			if strKey='GP:' then Thing[i].intGP := StrToInt(strValue);	// gun points (for long range wp.)
			if strKey='AP:' then Thing[i].intAP := StrToInt(strValue);	// armour points
			if strKey='SP:' then Thing[i].intSP := StrToInt(strValue);	// shovel points (for diggings)
			if strKey='Light:' then Thing[i].intLight := StrToInt(strValue);
			if strKey='Strength:' then Thing[i].intStrength := StrToInt(strValue);

			if strKey='DungeonLvl:' then Thing[i].intMinLvl := StrToInt(strValue);
			if strKey='SetItem:' then Thing[i].intRuneSet := StrToInt(strValue);

			if strKey='CharLvl:' then Thing[i].intCharLvl := StrToInt(strValue);
			if strKey='Profession:' then Thing[i].intProf := StrToInt(strValue);
            if strKey='Sex:' then Thing[i].intSex := StrToInt(strValue);

			if strKey='Textfile:' then Thing[i].strTextfile := strValue;

		end;
	end;

	Close (ItemFile);

	ItemCount:=i;
	//Writeln(IntToStr(ItemCount)+' items loaded.');

	// create a magic crystal for every chant
	for i:=1 to ChantCount do
	begin
		inc(ItemCount);
		Thing[ItemCount].strRealName:='Crystal of "'+Spell[i].strName+'"';
        Thing[ItemCount].strGenericName:='unknown crystal';
		Thing[ItemCount].strLearnChant:=Spell[i].strName;
		Thing[ItemCount].intSpellID:=i;
		Thing[ItemCount].intPrice:=Spell[i].intPP*25;
		Thing[ItemCount].chLetter:=chr(161);
		Thing[ItemCount].intCharLvl:=Spell[i].intCharLvl;
		Thing[ItemCount].strDescri:='This crystal allows you to learn "'+Spell[i].strName+'"';
		if Spell[i].intPP>8 then
			Thing[ItemCount].intProf:=2;
		if (Spell[i].intRefresh<6) or (Spell[i].intRefresh>40) or (Spell[i].intEffect=1) then
			Thing[ItemCount].blRare:=true
		else
			Thing[ItemCount].blShopOnly:=true;
		if Spell[i].intCharLvl>1 then
			Thing[ItemCount].intMinLvl:=Spell[i].intCharLvl-1
		else
			Thing[ItemCount].intMinLvl:=1;
	end;

    // make rare and unique items as well as all potions unidentified
    // all others get their real name
    for i:=1 to ItemCount do
    begin
        if (Thing[i].blRare = true) or (Thing[i].blUnique = true) or (Thing[i].blDrink = true) then
            Thing[i].blIdentified := false;

        if Thing[i].blIdentified = false then
            Thing[i].strName := Thing[i].strGenericName
        else
            Thing[i].strName := Thing[i].strRealName;
    end;

//     // save a table with all weapons
// 	Assign (ItemFile, 'data/weapon-overview.txt');
// 	Rewrite (ItemFile);
//     writeln(ItemFile, 'Name,WP,GP,AP,DLV,CLV,Credits');
//     for i:=1 to ItemCount do
//         if (Thing[i].blWield=true) and (Thing[i].blUnique=false) then
//             writeln(ItemFile, Thing[i].strRealName + ',' + IntToStr(Thing[i].intWP) + ',' + IntToStr(Thing[i].intGP) + ',' + IntToStr(Thing[i].intAP) + ',' + IntToStr(Thing[i].intMinLvl) + ',' + IntToStr(Thing[i].intCharLvl) + ',' + IntToStr(Thing[i].intPrice));
//     Close (ItemFile);
// 
//     // save a table with all clothes
// 	Assign (ItemFile, 'data/armour-overview.txt');
// 	Rewrite (ItemFile);
//     writeln(ItemFile, 'Name,WP,GP,AP,DLV,CLV,Credits');
//     for i:=1 to ItemCount do
//         if (Thing[i].blWear=true) and (Thing[i].blUnique=false) then
//             writeln(ItemFile, Thing[i].strRealName + ',' + IntToStr(Thing[i].intWP) + ',' + IntToStr(Thing[i].intGP) + ',' + IntToStr(Thing[i].intAP) + ',' + IntToStr(Thing[i].intMinLvl) + ',' + IntToStr(Thing[i].intCharLvl) + ',' + IntToStr(Thing[i].intPrice));
//     Close (ItemFile);
// 
//     // save a table with all hats
// 	Assign (ItemFile, 'data/hats-overview.txt');
// 	Rewrite (ItemFile);
//     writeln(ItemFile, 'Name,WP,GP,AP,DLV,CLV,Credits');
//     for i:=1 to ItemCount do
//         if (Thing[i].blHat=true) and (Thing[i].blUnique=false) then
//             writeln(ItemFile, Thing[i].strRealName + ',' + IntToStr(Thing[i].intWP) + ',' + IntToStr(Thing[i].intGP) + ',' + IntToStr(Thing[i].intAP) + ',' + IntToStr(Thing[i].intMinLvl) + ',' + IntToStr(Thing[i].intCharLvl) + ',' + IntToStr(Thing[i].intPrice));
//     Close (ItemFile);
// 
//     // save a table with all shoes
// 	Assign (ItemFile, 'data/shoes-overview.txt');
// 	Rewrite (ItemFile);
//     writeln(ItemFile, 'Name,WP,GP,AP,DLV,CLV,Credits');
//     for i:=1 to ItemCount do
//         if (Thing[i].blShoes=true) and (Thing[i].blUnique=false) then
//             writeln(ItemFile, Thing[i].strRealName + ',' + IntToStr(Thing[i].intWP) + ',' + IntToStr(Thing[i].intGP) + ',' + IntToStr(Thing[i].intAP) + ',' + IntToStr(Thing[i].intMinLvl) + ',' + IntToStr(Thing[i].intCharLvl) + ',' + IntToStr(Thing[i].intPrice));
//     Close (ItemFile);
// 
//     // save a table with all extras
// 	Assign (ItemFile, 'data/extras-overview.txt');
// 	Rewrite (ItemFile);
//     writeln(ItemFile, 'Name,WP,GP,AP,DLV,CLV,Credits');
//     for i:=1 to ItemCount do
//         if (Thing[i].blExtra=true) and (Thing[i].blUnique=false) then
//             writeln(ItemFile, Thing[i].strRealName + ',' + IntToStr(Thing[i].intWP) + ',' + IntToStr(Thing[i].intGP) + ',' + IntToStr(Thing[i].intAP) + ',' + IntToStr(Thing[i].intMinLvl) + ',' + IntToStr(Thing[i].intCharLvl) + ',' + IntToStr(Thing[i].intPrice));
//     Close (ItemFile);
// 
//     // save a table with all rings
// 	Assign (ItemFile, 'data/rings-overview.txt');
// 	Rewrite (ItemFile);
//     writeln(ItemFile, 'Name,WP,GP,AP,DLV,CLV,Credits');
//     for i:=1 to ItemCount do
//         if (Thing[i].blRing=true) and (Thing[i].blUnique=false) then
//             writeln(ItemFile, Thing[i].strRealName + ',' + IntToStr(Thing[i].intWP) + ',' + IntToStr(Thing[i].intGP) + ',' + IntToStr(Thing[i].intAP) + ',' + IntToStr(Thing[i].intMinLvl) + ',' + IntToStr(Thing[i].intCharLvl) + ',' + IntToStr(Thing[i].intPrice));
//     Close (ItemFile);
// 
//     // save a table with all unique items
// 	Assign (ItemFile, 'data/unique-items-overview.txt');
// 	Rewrite (ItemFile);
//     writeln(ItemFile, 'Name,WP,GP,AP,DLV,CLV,Credits');
//     for i:=1 to ItemCount do
//         if Thing[i].blUnique=true then
//             writeln(ItemFile, Thing[i].strRealName + ',' + IntToStr(Thing[i].intWP) + ',' + IntToStr(Thing[i].intGP) + ',' + IntToStr(Thing[i].intAP) + ',' + IntToStr(Thing[i].intMinLvl) + ',' + IntToStr(Thing[i].intCharLvl) + ',' + IntToStr(Thing[i].intPrice));
//     Close (ItemFile);



	Shops[1] := 'Food & Drinks';
	Shops[2] := 'Weapons & Tools';
	Shops[3] := 'the armour smith';
	Shops[4] := 'the library';
	Shops[5] := 'the hospital';
	Shops[6] := 'the restaurant';
	Shops[7] := 'the academy';
	Shops[8] := 'resource workshop';

	chBuilding[1] := chr(144);
	chBuilding[2] := chr(139);
	chBuilding[3] := chr(140);
	chBuilding[4] := chr(141);
	chBuilding[5] := chr(197);
	chBuilding[6] := chr(201);
	chBuilding[7] := chr(200);
	chBuilding[8] := chr(202);
end;

end.
