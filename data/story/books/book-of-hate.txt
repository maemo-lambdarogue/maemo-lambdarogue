1

Hate is understandable.
Though, hate is not the way for us to go.
At least, if we want to leave this place one day.
If we want to come back to Earth,
If we want to reach high into the skies.

Yes, the Drekh'Nar have won that war ages ago. They forced us
to live in caves, build tunnels through the darkness, where
monsters are just waiting for us, to kill us.

But undoubtedly: If we reach surface one day, we will have to
face the Drekh'Nar, and while they might still be dangerous,
and while they might try to force us back, we can't fight
against them.

It was hate that brought the war---a terrible misunderstanding
beget from fear and mistrust. We must not repeat that.

So here's the way to prevent hate for us all.