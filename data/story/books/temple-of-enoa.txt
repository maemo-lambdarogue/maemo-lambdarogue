The temple consists of four parts: the main hall with the enchanted well
in the north; the trader's atrium, a smaller hall west of the temple; the
entrance to the catacombs, east of the main hall; and the temple's library,
south of the catacomb's entrance. The altar building is southwest of the
temple's main area, surrounded by forest.

Several traders offer their services in the trader's atrium. Talk to them
if you want to buy equipment or if you need a healer.

If you want to enter the catacombs, be sure that you have a weapon and some
healing potions with you. Several wild animals, many of them dangerous, are
living there. You might also encounter trapdoors, leading to the sewers.

Beyond the sewers lies the old mine. If you go even deeper, you will reach
the lost outpost.

It is unknown what follows.
