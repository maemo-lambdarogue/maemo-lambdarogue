Tears are a mystery.

We cry because we love.
We cry because we hate.
We cry in joy, we cry in pain, and sometimes even the silence
of the dungeons seems to sound like tears, like water which is
flowing down the river.

Tears are reason and consequence.
They are reason for wars and reason for peace, and they follow
war and they follow peace.

Life therefore is a circle of Tears, and unlike some old religions
tell, we mustn't leave that circle. If we did, we are not men any
longer.
