Legend of the Undying King

Shortly after climbing down the steps to the dungeons of the Forgotten
Realm I felt observed. I saw shadows moving fast and hiding behind im-
pressive pillars. First I thought it might be the usual caveworms, but
then I recognized the shadows as men. I decided to be cordial and talk to
them. Indeed, they left the shadows---but not to talk.

They chased me through their dungeons. Somehow I got into the inner
realm and faced their king. His skin was unnaturally pale. Then a bell
rang twice. Suddenly undead came out of hidden corners and fell on
their knees in front of the king, swearing obedience to him. My perse-
cutors stopped and hailed the king, who started to bless every single
man. "In the name of Eris", he said, "the lost deity! To you, my people,
I swear loyalty, infinite in time and space!" Men and undead rejoiced.

It was a surreal scene, but it allowed me to escape. I think, however,
that the king saw me. His eyes rested on me for some seconds, but he
did not look angry. I think there was a great, although hidden, sadness
on his face. As if he wished to be freed. Freed from a curse, perhaps...

