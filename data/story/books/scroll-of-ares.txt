Many thousand years ago, Ares came to Earth, which was a planet most
interesting to him. The two dominating species tried to live in peace,
but they always failed. Men fought men, Drekh'Nar fought Drekh'Nar,
and of course men and Drekh'Nar also fought each other.

Ares was fascinated, but he doubted the sense of it all. People fought
chaotically. Although his sister Eris, deity of dispute and chaos, tried
to stop him, Ares showed himself to the people of Earth and gave them
rules for war. He also told them to stand on their own feet instead of
being used as toys by the other gods. Eris watched this with jealous
eyes.

After centuries, people found a way to preserve peace. If there was a
conflict with no other solution than war, men and Drekh'Nar bound them-
selves to the rules which were given to them by Ares, and some men star-
ted to worship Ares as "god of peace and war".

This worked for a long time. Conflicts became less, and while in former
times people had worshipped Eris and prayed for her support, they now pre-
ferred Ares---or no god at all.
