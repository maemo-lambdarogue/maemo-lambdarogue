You did not leave the temple area for quite awhile. Being a valley sur-
rounded by thick rock, life inside the temple is different from other parts
of NeoTerr, mankind's underground country. Now you're facing the wideness
of the outskirts of Enoa again.

After some hours of wandering, you hear a voice. You stop, glance around,
but nobody is there. You continue your walk, but some minutes later you
hear the voice again, clearer this time. "Help...me" it says, again and
again. It is the voice of an old man---weak, ill and sad. Again you try to
find the source of the words, but still without success. "Help me, young
wanderer", the voice continues. "Find the entrance to my realm and help me
to...overcome her..." And then it stops.

A realm? You only remember legends of the "Forgotten Realm"---actually its
entrance should be in the outskirts somewhere, but could the voice really
originate in this lost kingdom? It also said "my" realm---as if the king
himself had spoken to you.

Worried, but also excited, you decide to search for a way inside the For-
gotten Realm, as soon as you feel strong enough. In the meantime you
continue to walk through the beautiful wilderness...

