{
     Copyright (C) 2006-2008 by Mario Donick
     mario.donick@gmail.com    
                                                                            
     This program is free software; you can redistribute it and/or modify   
     it under the terms of the GNU General Public License as published by   
     the Free Software Foundation; either version 2 of the License, or      
     (at your option) any later version.                                    
                                                                            
     This program is distributed in the hope that it will be useful,        
     but WITHOUT ANY WARRANTY; without even the implied warranty of         
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
     GNU General Public License for more details.                           
                                                                            
     You should have received a copy of the GNU General Public License      
     along with this program; if not, write to the                          
     Free Software Foundation, Inc.,                                        
     59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.              
}

unit ExternMusic;


interface

uses
    Process, Crt, RandomArea;

function MusicPlaying: Boolean;
procedure PlayMusic(SoundFile: string);
procedure StopMusic;
procedure InitMusic;
procedure FreeMusic;


var
    ExtProgram: TProcess;
    blUseExternPlayer: Boolean;
    strExternPlayer: string;

implementation


function MusicPlaying: Boolean;
begin
    if ExtProgram<>nil then
        MusicPlaying := ExtProgram.Running;
end;


procedure StopMusic;
begin
    if ExtProgram.Running=true then
        ExtProgram.Terminate(0);
end;


procedure PlayMusic(SoundFile: string);
begin
    StopMusic;
    if blUseExternPlayer=true then
    begin
        ExtProgram.CommandLine := strExternPlayer+' '+PathData+'/music/'+Soundfile;
        //writeln('Executing '+ExtProgram.CommandLine);
        ExtProgram.Options := ExtProgram.Options + [poUsePipes];
        ExtProgram.Execute;
    end;
end;


procedure InitMusic;
begin
    ExtProgram := TProcess.Create(nil);
end;


procedure FreeMusic;
begin
    ExtProgram.free;
end;

end.
