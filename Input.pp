{
     Copyright (C) 2006-2008 by Mario Donick
     mario.donick@gmail.com

     This program is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation; either version 2 of the License, or
     (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program; if not, write to the
     Free Software Foundation, Inc.,
     59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
}

unit Input;


interface

uses
    Constants, ExternMusic, ExternSFX, SDL, Crt, Keyboard, Video, SysUtils, RandomArea, Player, Items, Chants, BaseOutput;


var
    MyEvent: TSDL_EVENT;
    xMouse, yMouse: Integer;
    KeyRepeatSoll, KeyRepeatIst: Integer;

function GetDirection(mode: integer): char;
function GetRepeat(mode: integer): integer;
function GetTextInput(message: string; MaxLength: integer): string;
function GetNumericalInput(message: string; MaxLength: integer): integer;
function GetKeyInput(message: string; pressanykey: boolean): string;

function TransformClickInKey(x, y: integer): integer;
function MouseInventoryAction(x, y, n: integer): char;
function MouseSexSelect(x, y: integer): char;
function MouseDiffSelect(x, y: integer): char;
function MousePresetSelect(x, y: integer): char;
function MouseTitleSelect(x, y: integer): char;
function MouseGameMenuSelect(x, y: integer): char;
function MouseGodSelect(x, y: integer): char;
function MouseInventoryItem(x, y: integer; IsShop: boolean; ShopID: integer): integer;
function MouseSpellbookAction(x, y: integer): char;
function MouseShopAction(x, y: integer): char;
function MouseStealAction(x, y: integer): char;
function MouseProfSelect(x, y: integer): char;
function MouseProfSelectM(x, y: integer): char;
function MouseProfSelectF(x, y: integer): char;
function MouseSkillSelect(x, y: integer): char;
function MouseAcceptQuest: char;
function MouseWorkshop: char;
function MouseGameOver: char;


function cookKey(unicode, sdlkey: integer): integer;
procedure SaveConfig;
procedure CreateDefaultConfig;
procedure InitKeys(b: boolean);
function ConsoleInput(): integer;
function BoolString(b: Boolean; m: Integer): string;
function BoolToggle(b: Boolean): Boolean;

implementation

// returns "true"/"false" or "yes"/"no" as String
function BoolString(b: Boolean; m: Integer): string;
begin
    if m=0 then
    begin
        if b=true then
            BoolString:='True'
        else
            BoolString:='False';
    end
    else
    begin
        if b=true then
            BoolString:='Yes'
        else
            BoolString:='No';
    end;
end;


// swaps the true/false string of the given boolean
function BoolToggle(b: Boolean): Boolean;
begin
    BoolToggle:=not(b);
end;


procedure SaveConfig;
var
    KeyFile: TextFile;
begin
    Assign(KeyFile, PathUserWritable+'lambdarogue.cfg');
    Rewrite(KeyFile);

    Writeln(KeyFile, '# LambdaRogue Configuration File');
    Writeln(KeyFile, '# created by LambdaRogue '+strVersion);
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, '# Visual Options');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'UseSDL');
    Writeln(KeyFile, '= '+BoolString(UseSDL, 0));
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'SmallTiles');
    Writeln(KeyFile, '= '+BoolString(UseSmallTiles, 0));
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'Fullscreen');
    Writeln(KeyFile, '= '+BoolString(UseFullScreen, 0));
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, '1024x768');
    Writeln(KeyFile, '= '+BoolString(UseHiRes, 0));
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'CropForNetbook');
    Writeln(KeyFile, '= '+BoolString(Netbook, 0));
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'ShowBlood');
    Writeln(KeyFile, '= '+BoolString(ShowBlood, 0));
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, '# Gameplay Options');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'GrowingMonsters');
    Writeln(KeyFile, '= '+BoolString(GrowingMonsters, 0));
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'MoveAfterKill');
    Writeln(KeyFile, '= '+BoolString(AutoMoveToTile, 0));
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, '# Music Options');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'PlayMusic');
    Writeln(KeyFile, '= '+BoolString(blUseExternPlayer, 0));
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'PlaySoundFX');
    Writeln(KeyFile, '= '+BoolString(blUseExternPlayerTwo, 0));
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'MediaPlayer:');
    Writeln(KeyFile, strExternPlayer);
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, '# Interface Options');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'UseMouse');
    Writeln(KeyFile, '= '+BoolString(UseMouseToMove, 0));
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'MoveOneTilePerClick');
    Writeln(KeyFile, '= '+BoolString(MoveOneTilePerClick, 0));
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, '# Keybindings');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'MoveNorth:');
    Writeln(KeyFile, KeyNorth);
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'MoveSouth:');
    Writeln(KeyFile, KeySouth);
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'MoveEast:');
    Writeln(KeyFile, KeyEast);
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'MoveWest:');
    Writeln(KeyFile, KeyWest);
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'MoveNorthEast:');
    Writeln(KeyFile, KeyNorthEast);
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'MoveNorthWest:');
    Writeln(KeyFile, KeyNorthWest);
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'MoveSouthEast:');
    Writeln(KeyFile, KeySouthEast);
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'MoveSouthWest:');
    Writeln(KeyFile, KeySouthWest);
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'GeneralAction:');
    Writeln(KeyFile, KeyEnter);
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'TalkAndTrade:');
    Writeln(KeyFile, KeyTrade);
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'CloseDoor:');
    Writeln(KeyFile, KeyCloseDoor);
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'Dig:');
    Writeln(KeyFile, KeyTunnel);
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'Take:');
    Writeln(KeyFile, KeyTake);
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'SearchAndSteal:');
    Writeln(KeyFile, KeySearchSteal);
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'Pray:');
    Writeln(KeyFile, KeyPray);
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'ShootWeapon:');
    Writeln(KeyFile, KeyShoot);
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'Songbook:');
    Writeln(KeyFile, KeyChant);
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'ChantLastSong:');
    Writeln(KeyFile, KeyChantLast);
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'RestOneTurn:');
    Writeln(KeyFile, KeyShortRest);
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'Rest:');
    Writeln(KeyFile, KeyRest);
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'ShowStatus:');
    Writeln(KeyFile, KeyStatus);
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'ShowQuestlog:');
    Writeln(KeyFile, KeyQuestlog);
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'ShowInventory:');
    Writeln(KeyFile, KeyInventory);
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'SetQuickKeys:');
    Writeln(KeyFile, KeySetQuickKeys);
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'ShowHelp:');
    Writeln(KeyFile, KeyHelp);
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'Map:');
    Writeln(KeyFile, KeyBigMap);
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'IdentifyTile:');
    Writeln(KeyFile, KeyLook);
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'GameMenu:');
    Writeln(KeyFile, KeyQuit);
    Writeln(KeyFile, ' ');

    Close(KeyFile);
end;


procedure CreateDefaultConfig;
var
    KeyFile: TextFile;
begin
    Assign(KeyFile, PathUserWritable+'lambdarogue.cfg');
    Rewrite(KeyFile);

    Writeln(KeyFile, '# LambdaRogue Configuration File');
    Writeln(KeyFile, '# created by LambdaRogue '+strVersion);
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, '# Visual Options');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'UseSDL');
    Writeln(KeyFile, '= True');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'SmallTiles');
    Writeln(KeyFile, '= True');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'Fullscreen');
    Writeln(KeyFile, '= True');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, '1024x768');
    Writeln(KeyFile, '= False');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'CropForNetbook');
    Writeln(KeyFile, '= False');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'ShowBlood');
    Writeln(KeyFile, '= True');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, '# Gameplay Options');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'GrowingMonsters');
    Writeln(KeyFile, '= True');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'MoveAfterKill');
    Writeln(KeyFile, '= False');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, '# Music Options');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'PlayMusic');
    Writeln(KeyFile, '= False');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'PlaySoundFX');
    Writeln(KeyFile, '= False');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'MediaPlayer:');
    Writeln(KeyFile, 'mplayer 2> /dev/null > /dev/null');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, '# Interface Options');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'UseMouse');
    Writeln(KeyFile, '= True');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'MoveOneTilePerClick');
    Writeln(KeyFile, '= True');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, '# Keybindings');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'MoveNorth:');
    Writeln(KeyFile, 'k');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'MoveSouth:');
    Writeln(KeyFile, 'j');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'MoveEast:');
    Writeln(KeyFile, 'l');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'MoveWest:');
    Writeln(KeyFile, 'h');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'MoveNorthEast:');
    Writeln(KeyFile, 'u');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'MoveNorthWest:');
    Writeln(KeyFile, 'y');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'MoveSouthEast:');
    Writeln(KeyFile, 'n');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'MoveSouthWest:');
    Writeln(KeyFile, 'b');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'GeneralAction:');
    Writeln(KeyFile, 'U');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'TalkAndTrade:');
    Writeln(KeyFile, 't');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'CloseDoor:');
    Writeln(KeyFile, 'D');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'Dig:');
    Writeln(KeyFile, 'd');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'Take:');
    Writeln(KeyFile, 'g');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'SearchAndSteal:');
    Writeln(KeyFile, 'S');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'Pray:');
    Writeln(KeyFile, 'p');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'ShootWeapon:');
    Writeln(KeyFile, 'f');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'Songbook:');
    Writeln(KeyFile, 'm');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'ChantLastSong:');
    Writeln(KeyFile, 'c');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'RestOneTurn:');
    Writeln(KeyFile, '.');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'Rest:');
    Writeln(KeyFile, 'r');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'ShowStatus:');
    Writeln(KeyFile, 's');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'ShowQuestlog:');
    Writeln(KeyFile, 'L');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'ShowInventory:');
    Writeln(KeyFile, 'i');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'SetQuickKeys:');
    Writeln(KeyFile, 'C');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'ShowHelp:');
    Writeln(KeyFile, '?');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'Map:');
    Writeln(KeyFile, 'M');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'IdentifyTile:');
    Writeln(KeyFile, 'I');
    Writeln(KeyFile, ' ');
    Writeln(KeyFile, 'GameMenu:');
    Writeln(KeyFile, 'Q');
    Writeln(KeyFile, ' ');

    Close(KeyFile);
end;

procedure InitKeys(b: boolean);
var
    KeyFile: TextFile;
    strKey, strValue: string;
begin

    if b=true then
    begin
        UseSDL:=false;
        UseHiRes:=false;
        Netbook:=false;
        UseFullScreen:=false;
        UseMouseToMove:=false;
        GrowingMonsters:=true;
        MoveOneTilePerClick:=false;
        blUseExternPlayer:=false;
        blUseExternPlayerTwo:=false;
        AutoMoveToTile:=false;
        UseSmallTiles:=false;
        KeyNorth := 'k';
        KeySouth := 'j';
        KeyEast := 'l';
        KeyWest := 'h';
        KeyNorthEast := 'u';
        KeySouthEast := 'n';
        KeyNorthWest := 'y';
        KeySouthWest := 'b';
        KeyEnter := 'U';
        KeyTrade := 't';
        KeyTake := 'g';
        KeyCloseDoor := 'D';
        KeyTunnel := 'd';
        KeyPray := 'p';
        KeyShoot := 'f';
        KeyChant := 'm';
        KeyShortRest := '.';
        KeyRest := 'r';
        KeyStatus := 's';
        KeySearchSteal := 'S';
        KeyQuestlog := 'L';
        KeyInventory := 'i';
        KeyHelp := '?';
        KeyLook := 'I';
        KeyQuit := 'Q';
        KeyChantLast := 'c';
        KeySetQuickKeys := 'C';
        KeyBigMap:='M';
    end;


    // WriteLn ('reading config file ...');

    Assign(KeyFile, PathUserWritable+'lambdarogue.cfg');
    Reset(KeyFile);

    while eof(KeyFile)=false do
    begin
        repeat
            ReadLn(KeyFile, strKey);
            strKey:=trim(lowercase(strKey));
        until ((strKey<>'') and (strKey[1]<>'#')) or (eof(KeyFile));

        ReadLn(KeyFile, strValue);
        strValue:=trim(strValue);

//         strValue:=lowercase(strValue);

//         WriteLn('read '+strKey+' '+strValue);

        // movement
        if strKey='movenorth:' then
            KeyNorth := strValue[1];
        if strKey='movesouth:' then
            KeySouth := strValue[1];
        if strKey='moveeast:' then
            KeyEast := strValue[1];
        if strKey='movewest:' then
            KeyWest := strValue[1];
        if strKey='movenortheast:' then
            KeyNorthEast := strValue[1];
        if strKey='movesoutheast:' then
            KeySouthEast := strValue[1];
        if strKey='movenorthwest:' then
            KeyNorthWest := strValue[1];
        if strKey='movesouthwest:' then
            KeySouthWest := strValue[1];

        // interaction
        if strKey='generalaction:' then
            KeyEnter := strValue[1];

        if strKey='talkandtrade:' then
            KeyTrade := strValue[1];
        if strKey='searchandsteal:' then
            KeySearchSteal := strValue[1];
        if strKey='closedoor:' then
            KeyCloseDoor := strValue[1];
        if strKey='dig:' then
            KeyTunnel := strValue[1];
        if strKey='take:' then
            KeyTake := strValue[1];
        if strKey='setquickkeys:' then
            KeySetQuickKeys := strValue[1];

        if strKey='pray:' then
            KeyPray := strValue[1];
        if strKey='shootweapon:' then
            KeyShoot := strValue[1];
        if strKey='songbook:' then
            KeyChant := strValue[1];
        if strKey='chantlastsong:' then
            KeyChantLast := strValue[1];
        if strKey='restoneturn:' then
            KeyShortRest := strValue[1];
        if strKey='rest:' then
            KeyRest := strValue[1];

        if strKey='showstatus:' then
            KeyStatus := strValue[1];
        if strKey='showquestlog:' then
            KeyQuestlog := strValue[1];
        if strKey='showinventory:' then
            KeyInventory := strValue[1];

        if strKey='showhelp:' then
            KeyHelp := strValue[1];
        if strKey='map:' then
            KeyBigMap := strValue[1];
        if strKey='identifytile:' then
            KeyLook := strValue[1];

        if strKey='gamemenu:' then
            KeyQuit := strValue[1];

        // also read options
        if b=true then
        begin
            if (strKey='usesdl') and (lowercase(strValue)='= true') then
                UseSDL:=true;
            if (strKey='usesdl') and (lowercase(strValue)='= false') then
                UseSDL:=false;

            if (strKey='1024x768') and (lowercase(strValue)='= true') then
                UseHiRes:=true;
            if (strKey='1024x768') and (lowercase(strValue)='= false') then
                UseHiRes:=false;

            if (strKey='cropfornetbook') and (lowercase(strValue)='= true') then
                Netbook:=true;
            if (strKey='cropfornetbook') and (lowercase(strValue)='= false') then
                Netbook:=false;

            if (strKey='smalltiles') and (lowercase(strValue)='= true') then
                UseSmallTiles:=true;
            if (strKey='smalltiles') and (lowercase(strValue)='= false') then
                UseSmallTiles:=false;

            if (strKey='fullscreen') and (lowercase(strValue)='= true') then
                UseFullScreen:=true;
            if (strKey='fullscreen') and (lowercase(strValue)='= false') then
                UseFullScreen:=false;

            if (strKey='showblood') and (lowercase(strValue)='= true') then
                ShowBlood := true;
            if (strKey='showblood') and (lowercase(strValue)='= false') then
                ShowBlood := false;

            //if (strKey='usemouse') and (lowercase(strValue)='= true') then
                //UseMouseToMove := true;
            //if (strKey='usemouse') and (lowercase(strValue)='= false') then
                //UseMouseToMove := false; 
            UseMouseToMove := false;

            if (strKey='growingmonsters') and (lowercase(strValue)='= true') then
                GrowingMonsters := true;
            if (strKey='growingmonsters') and (lowercase(strValue)='= false') then
                GrowingMonsters := false;

            if (strKey='moveafterkill') and (lowercase(strValue)='= true') then
                AutoMoveToTile := true;
            if (strKey='moveafterkill') and (lowercase(strValue)='= false') then
                AutoMoveToTile := false;

            if (strKey='moveonetileperclick') and (lowercase(strValue)='= true') then
                MoveOneTilePerClick := true;
            if (strKey='moveonetileperclick') and (lowercase(strValue)='= false') then
                MoveOneTilePerClick := false;

            if (strKey='playmusic') and (lowercase(strValue)='= true') then
                blUseExternPlayer := true;
            if (strKey='playmusic') and (lowercase(strValue)='= false') then
                blUseExternPlayer := false;

            if (strKey='playsoundfx') and (lowercase(strValue)='= true') then
                blUseExternPlayerTwo := true;
            if (strKey='playsoundfx') and (lowercase(strValue)='= false') then
                blUseExternPlayerTwo := false;

            if strKey='mediaplayer:' then
                strExternPlayer := strValue;
        end;
    end;

    Close(KeyFile);
end;

// this function returns the key used in the gameover screen (either KeyEnter or space)
function MouseGameOver: char;
var
    ch: char;
    Knoepfe, KX, KY: Longint;
begin
    BottomBar;
    ShowMessage('Left-click to create character dump, right-click to quit.', false);
    ch:='-';
    repeat
        begin
            if SDL_WaitEvent( @MyEvent ) > 0 then
            begin
                KX:=MyEvent.motion.x;
                KY:=MyEvent.motion.y;
                Knoepfe := SDL_GetMouseState(KX, KY);
                if (Knoepfe and SDL_BUTTON(SDL_BUTTON_LEFT))<>0 then
                    ch:='d';
                if (Knoepfe and SDL_BUTTON(SDL_BUTTON_RIGHT))<>0 then
                    ch:=' ';
            end;
        end;
    until (ch='d') or (ch=' ');

    MouseGameOver:=ch;
end;


// this function returns the key used in a quest screen (either KeyEnter or space)
function MouseAcceptQuest: char;
var
    ch: char;
    Knoepfe, KX, KY: Longint;
begin
    ShowMessage('Left-click to accept quest, right-click or [ESC] to leave.', false);
    ch:='-';
    repeat
        begin
            if SDL_WaitEvent( @MyEvent ) > 0 then
            begin
                KX:=MyEvent.motion.x;
                KY:=MyEvent.motion.y;
                Knoepfe := SDL_GetMouseState(KX, KY);
                if (Knoepfe and SDL_BUTTON(SDL_BUTTON_LEFT))<>0 then
                    ch:=KeyEnter;
                if (Knoepfe and SDL_BUTTON(SDL_BUTTON_RIGHT))<>0 then
                    ch:=' ';

                if MyEvent.type_ = SDL_KEYDOWN then
                    if cookKey(MyEvent.key.keysym.unicode, MyEvent.key.keysym.sym) = SDLK_ESCAPE then
                        ch:=' ';
            end;
        end;
    until (ch=KeyEnter) or (ch=' ');

    MouseAcceptQuest:=ch;
end;


// this function returns the key used in resource workshop (either KeyEnter or space)
function MouseWorkshop: char;
var
    ch: char;
    Knoepfe, KX, KY: Longint;
begin
    ShowMessage('Left-click to sell a resource, right-click to leave.', false);
    ch:='-';
    repeat
        begin
            if SDL_WaitEvent( @MyEvent ) > 0 then
            begin
                KX:=MyEvent.motion.x;
                KY:=MyEvent.motion.y;
                Knoepfe := SDL_GetMouseState(KX, KY);
                if (Knoepfe and SDL_BUTTON(SDL_BUTTON_LEFT))<>0 then
                    ch:='s';
                if (Knoepfe and SDL_BUTTON(SDL_BUTTON_RIGHT))<>0 then
                    ch:=' ';
            end;
        end;
    until (ch='s') or (ch=' ');

    MouseWorkshop:=ch;
end;


// this function returns the key the player would have pressed in spellbook when not using the mouse
function MouseSpellbookAction(x, y: integer): char;
begin
    MouseSpellbookAction:=' ';

//     writeln(IntToStr(x)+'/'+IntToStr(y));
    // check if we are in inventory popup menu
    if (x>133) and (x<526) then
    begin
        if (y>=160) and (y<=399) then
        begin
            if (y>=220) and (y<=239) then
                MouseSpellbookAction:='c';
            if (y>=240) and (y<=259) then
                MouseSpellbookAction:='i';
        end;
        if (x>=150) and (y>=280) and (x<=209) and (y<=299) then
            MouseSpellbookAction:=' ';
    end;
    ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
end;


// this function returns the key the player would have pressed in shop when not using the mouse
function MouseShopAction(x, y: integer): char;
begin
//     writeln(IntToStr(x)+'/'+IntToStr(y));
    // check if we are in inventory popup menu
    if (x>133) and (x<526) then
    begin
        if (y>=160) and (y<=399) then
        begin
            if (y>=220) and (y<=239) then
                MouseShopAction:='g';
            if (y>=240) and (y<=259) then
                MouseShopAction:='l';
        end;
        if (x>=150) and (y>=280) and (x<=209) and (y<=299) then
            MouseShopAction:=' ';
    end;
    ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
end;


// this function returns the key the player would have pressed in a steal action when not using the mouse
function MouseStealAction(x, y: integer): char;
begin
//     writeln(IntToStr(x)+'/'+IntToStr(y));
    // check if we are in inventory popup menu
    if (x>133) and (x<526) then
    begin
        if (y>=160) and (y<=399) then
        begin
            if (y>=220) and (y<=239) then
                MouseStealAction:='s';
            if (y>=240) and (y<=259) then
                MouseStealAction:='l';
        end;
        if (x>=150) and (y>=280) and (x<=209) and (y<=299) then
            MouseStealAction:=' ';
    end;
    ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
end;


// this function returns the key the player would have pressed in inventory when not using the mouse
function MouseInventoryAction(x, y, n: integer): char;
begin
    MouseInventoryAction:=' ';
//     writeln(IntToStr(x)+'/'+IntToStr(y));
    // check if we are in inventory popup menu
    if (x>133) and (x<556) then
    begin
        if (y>=160) and (y<=399) then
        begin
            if (y>=220) and (y<=239) then
                if (Thing[Inventory[n].intType].blWield=true) or (Thing[Inventory[n].intType].blWear=true) or (Thing[Inventory[n].intType].blHat=true) or (Thing[Inventory[n].intType].blRing=true) then
                    if (ThePlayer.intWeapon<>Inventory[n].intType) and (ThePlayer.intArmour<>Inventory[n].intType) and (ThePlayer.intHat<>Inventory[n].intType) and (ThePlayer.intRingLeft<>Inventory[n].intType) and (ThePlayer.intRingRight<>Inventory[n].intType) then
                        MouseInventoryAction:='e';

            if (y>=240) and (y<=259) then
                if (Thing[Inventory[n].intType].blWield=true) or (Thing[Inventory[n].intType].blWear=true) or (Thing[Inventory[n].intType].blHat=true) or (Thing[Inventory[n].intType].blRing=true) then
                    if (ThePlayer.intWeapon=Inventory[n].intType) or (ThePlayer.intArmour=Inventory[n].intType) or (ThePlayer.intHat=Inventory[n].intType) or (ThePlayer.intRingLeft=Inventory[n].intType) or (ThePlayer.intRingRight=Inventory[n].intType) then
                        MouseInventoryAction:='r';

            if (y>=260) and (y<=279) then
                MouseInventoryAction:='d';
            if (y>=280) and (y<=299) then
                MouseInventoryAction:='l';

            if (y>=300) and (y<=319) then
                if (Thing[Inventory[n].intType].strTextfile<>'-') or (Thing[Inventory[n].intType].chLetter=chr(161)) then
                    MouseInventoryAction:='s';

            if (y>=320) and (y<=339) then
                if (Thing[Inventory[n].intType].blEat=true) or (Thing[Inventory[n].intType].blDrink=true) then
                    MouseInventoryAction:='c';

        end;
        if (x>=150) and (y>=360) and (x<=209) and (y<=379) then
            MouseInventoryAction:=' ';
    end;
    ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
end;


// this function returns the key the player would have pressed in sex selection screen when not using the mouse
function MouseSexSelect(x, y: integer): char;
begin
    if (x>=180) and (y>=150) and (x<=415) and (y<=450) then
        MouseSexSelect:='1'; // male
    if (x>=420) and (y>=165) and (x<=595) and (y<=450) then
        MouseSexSelect:='2'; // female
end;


// this function returns the number of the selected profession
function MouseProfSelect(x, y: integer): char;
begin
    MouseProfSelect:=' ';
    // check if we are in selection window
    if (x>64) and (x<742) then
        if (y>=100) and (y<=419) then
        begin
            if (y>=220) and (y<=239) then
                MouseProfSelect:='1';
            if (y>=240) and (y<=259) then
                MouseProfSelect:='2';
            if (y>=260) and (y<=279) then
                MouseProfSelect:='3';
            if (y>=280) and (y<=299) then
                MouseProfSelect:='4';
            if (y>=300) and (y<=319) then
                MouseProfSelect:='5';
        end;
end;


function MouseDiffSelect(x, y: integer): char;
begin
    MouseDiffSelect:=' ';
    if (x>=380) and (y>=125) and (x<=485) and (y<=180) then
        MouseDiffSelect:='1'; // bronze
    if (x>=220) and (y>=245) and (x<=320) and (y<=295) then
        MouseDiffSelect:='2'; // silver
    if (x>=465) and (y>=350) and (x<=550) and (y<=405) then
        MouseDiffSelect:='3'; // gold
end;


function MousePresetSelect(x, y: integer): char;
begin
    MousePresetSelect:=' ';
    if (x>=320) and (y>=50) and (x<=480) and (y<=200) then
        MousePresetSelect:='1'; // male knight
    if (x>=45) and (y>=190) and (x<=320) and (y<=440) then
        MousePresetSelect:='2'; // female archer
    if (x>=420) and (y>=355) and (x<=640) and (y<=500) then
        MousePresetSelect:='3'; // male battlemage
    if (x>=545) and (y>=95) and (x<=755) and (y<=305) then
        MousePresetSelect:='4'; // female monk
end;

function MouseTitleSelect(x, y: integer): char;
begin
    MouseTitleSelect:=' ';
    if (x>=360) and (y>=400) and (x<=484) and (y<=423) then
        MouseTitleSelect:='1'; // normal start
    if (x>=380) and (y>=428) and (x<=465) and (y<=447) then
        MouseTitleSelect:='2'; // quickstart
    if (x>=330) and (y>=455) and (x<=515) and (y<=476) then
        MouseTitleSelect:='3'; // load game
    if (x>=404) and (y>=480) and (x<=442) and (y<=499) then
        MouseTitleSelect:='4'; // quit
end;


function MouseGameMenuSelect(x, y: integer): char;
begin
    MouseGameMenuSelect:=' ';
    if (x>=325) and (y>=205) and (x<=475) and (y<=230) then
        MouseGameMenuSelect:='1'; // general options
    if (x>=345) and (y>=238) and (x<=455) and (y<=260) then
        MouseGameMenuSelect:='2'; // keybindings
    if (x>=335) and (y>=265) and (x<=465) and (y<=290) then
        MouseGameMenuSelect:='3'; // save and quit
end;


function MouseGodSelect(x, y: integer): char;
begin
    MouseGodSelect:=' ';
    if (x>=120) and (y>=60) and (x<=220) and (y<=185) then
        MouseGodSelect:='1'; // aphrodite
    if (x>=280) and (y>=65) and (x<=405) and (y<=190) then
        MouseGodSelect:='2'; // hermes
    if (x>=445) and (y>=90) and (x<=555) and (y<=225) then
        MouseGodSelect:='3'; // apoll
    if (x>=560) and (y>=210) and (x<=665) and (y<=335) then
        MouseGodSelect:='4'; // dionysos
    if (x>=570) and (y>=345) and (x<=705) and (y<=490) then
        MouseGodSelect:='5'; // apoll
end;


function MouseProfSelectM(x, y: integer): char;
begin
    MouseProfSelectM:=' ';
    if (x>=30) and (y>=150) and (x<=155) and (y<=310) then
        MouseProfSelectM:='1'; // constructor
    if (x>=175) and (y>=160) and (x<=305) and (y<=330) then
        MouseProfSelectM:='2'; // enchanter
    if (x>=325) and (y>=170) and (x<=440) and (y<=315) then
        MouseProfSelectM:='3'; // thief
    if (x>=465) and (y>=175) and (x<=605) and (y<=305) then
        MouseProfSelectM:='4'; // ranger
    if (x>=645) and (y>=170) and (x<=780) and (y<=290) then
        MouseProfSelectM:='5'; // soldier
end;

function MouseProfSelectF(x, y: integer): char;
begin
    MouseProfSelectF:=' ';
    if (x>=0) and (y>=145) and (x<=160) and (y<=320) then
        MouseProfSelectF:='1'; // constructor
    if (x>=161) and (y>=85) and (x<=320) and (y<=320) then
        MouseProfSelectF:='2'; // enchanter
    if (x>=340) and (y>=105) and (x<=430) and (y<=255) then
        MouseProfSelectF:='3'; // thief
    if (x>=431) and (y>=60) and (x<=674) and (y<=300) then
        MouseProfSelectF:='4'; // ranger
    if (x>=675) and (y>=180) and (x<=800) and (y<=350) then
        MouseProfSelectF:='5'; // soldier
end;

function MouseSkillSelect(x, y: integer): char;
begin
    MouseSkillSelect:=' ';

    // are we in skills area?
    if (x>=228) and (x<=475) then
    begin

        // abilities
        if (y>=199) and (y<=246) then
        begin
            if (x>=228) and (x<=275) then
                MouseSkillSelect:='1';  // Fight
            if (x>=278) and (x<=325) then
                MouseSkillSelect:='2';  // View
            if (x>=328) and (x<=375) then
                MouseSkillSelect:='3';  // Chant
            if (x>=378) and (x<=425) then
                MouseSkillSelect:='4';  // Move
            if (x>=428) and (x<=475) then
                MouseSkillSelect:='5';  // Burgle
        end;


        // skills
        if (y>=259) and (y<=306) then
        begin
            if (x>=228) and (x<=275) then
                MouseSkillSelect:='6';  // Sword
            if (x>=278) and (x<=325) then
                MouseSkillSelect:='7';  // Axe
            if (x>=328) and (x<=375) then
                MouseSkillSelect:='8';  // Lance
            if (x>=378) and (x<=425) then
                MouseSkillSelect:='9';  // Firearm
            if (x>=428) and (x<=475) then
                MouseSkillSelect:='0';  // Tool
        end;


        // talents
        if (y>=319) and (y<=366) then
        begin
            if (x>=228) and (x<=275) then
                MouseSkillSelect:='h';  // Humility
            if (x>=278) and (x<=325) then
                MouseSkillSelect:='t';  // Trade
        end;

    end;
end;


// this function returns the number of an item selected in inventory or shop (-2: Close inventory)
function MouseInventoryItem(x, y: integer; IsShop: boolean; ShopID: integer): integer;
begin
    MouseInventoryItem:=-1;
//     writeln(IntToStr(x)+'/'+IntToStr(y));

    // check if we are in inventory window
    if (x>64) and (x<742) then
        if (y>=100) and (y<=419) then
        begin
            if (y>=100) and (y<=119) then
                MouseInventoryItem:=1;
            if (y>=120) and (y<=139) then
                MouseInventoryItem:=2;
            if (y>=140) and (y<=159) then
                MouseInventoryItem:=3;
            if (y>=160) and (y<=179) then
                MouseInventoryItem:=4;
            if (y>=180) and (y<=199) then
                MouseInventoryItem:=5;
            if (y>=200) and (y<=219) then
                MouseInventoryItem:=6;
            if (y>=220) and (y<=239) then
                MouseInventoryItem:=7;
            if (y>=240) and (y<=259) then
                MouseInventoryItem:=8;
            if (y>=260) and (y<=279) then
                MouseInventoryItem:=9;
            if (y>=280) and (y<=299) then
                MouseInventoryItem:=10;
            if (y>=300) and (y<=319) then
                MouseInventoryItem:=11;
            if (y>=320) and (y<=339) then
                MouseInventoryItem:=12;
            if (y>=340) and (y<=359) then
                MouseInventoryItem:=13;
            if (y>=360) and (y<=379) then
                MouseInventoryItem:=14;
            if (y>=380) and (y<=399) then
                MouseInventoryItem:=15;
            if (y>=400) and (y<=419) then
                MouseInventoryItem:=16;
        end;

    if IsShop=false then
    begin
        if ShopID=0 then    // for inventory
            if (MouseInventoryItem>-1) and (Inventory[MouseInventoryItem].longNumber<1) then
                MouseInventoryItem:=-1;

        if ShopID=-1 then    // for songbook
            if (MouseInventoryItem>-1) and (Spellbook[MouseInventoryItem].intKnown=0) then
                MouseInventoryItem:=-1;

        if ShopID=-2 then    // for resource workshop
            if (MouseInventoryItem>6) then
                MouseInventoryItem:=-1;

        if ShopID=-3 then    // for academy
            if (MouseInventoryItem>2) then
                MouseInventoryItem:=-1;
    end
    else
    begin
        if (MouseInventoryItem>-1) and (MyShop[ShopID].Inventory[MouseInventoryItem].intType=0) then
            MouseInventoryItem:=-1;
    end;

    if MouseInventoryItem>-1 then
        ShowDungeon(ThePlayer.intX, ThePlayer.intY, 80, 25, 0);
end;

// ' returns the distance between two points
function Distance(x0, y0, x1, y1: real): integer;
var
    xd, yd: real;
    d: real;
begin
//     writeln(x0);
//     writeln(x1);
//     writeln(y0);
//     writeln(y1);

    xd:=(x1-x0) * (x1-x0);
    yd:=(y1-y0) * (y1-y0);
    d:=abs(xd+yd);
//     writeln(d);
    if d=0 then
        Distance:=0
    else
    begin
        d:=sqrt(d);
        Distance:=abs(trunc(d));
    end;
end;

// This function returns the number of keypressed need to reach the given position
function GetKeyRepeat(x, y, k: integer): integer;
var
    LeftX, RightX, TopY, BottomY, Width, Height: Integer;
begin
    if UseSmallTiles=true then
    begin
        LeftX:=400;
        RightX:=421;
        TopY:=240;
        BottomY:=280;
        Width:=20;
        Height:=40;
    end
    else
    begin
        LeftX:=400;
        RightX:=441;
        TopY:=240;
        BottomY:=320;
        Width:=40;
        Height:=80;
    end;

    GetKeyRepeat:=1;
    if MoveOneTilePerClick=false then
    begin
        case k of
            SDLK_LEFT:
                GetKeyRepeat:=((LeftX-x) div Width)+1;
            SDLK_RIGHT:
                GetKeyRepeat:=((x-RightX) div Width)+1;
            SDLK_UP:
                GetKeyRepeat:=((TopY-y) div Height)+1;
            SDLK_DOWN:
                GetKeyRepeat:=((y-BottomY) div Height)+1;

            SDLK_KP7:
                GetKeyRepeat:=(Distance(LeftX, TopY, x, y) div Height)+1;
            SDLK_KP9:
                GetKeyRepeat:=(Distance(RightX-1, TopY, x, y) div Height)+1;
            SDLK_KP1:
                GetKeyRepeat:=(Distance(LeftX, BottomY, x, y) div Height)+1;
            SDLK_KP3:
                GetKeyRepeat:=(Distance(RightX-1, BottomY, x, y) div Height)+1;

        end;
    end;

end;


function TransformClickInKey(x, y: integer): integer;
var
    k: integer;
    LeftX, RightX, TopY, BottomY: integer;
begin

      //writeln(IntToStr(x)+'/'+IntToStr(y));

// 400/240 - 421/280: 20x40

// 400/240 - 441/320: 40x80


    if UseSmallTiles=true then
    begin
        LeftX:=400;
        RightX:=421;
        TopY:=240;
        BottomY:=280;
    end
    else
    begin
        LeftX:=400;
        RightX:=441;
        TopY:=240;
        BottomY:=320;
    end;


    if x<LeftX then
    begin
        k:=SDLK_LEFT;
        if y<TopY then
            k:=SDLK_KP7;
        if y>BottomY then
            k:=SDLK_KP1;
    end;

    if x>RightX then
    begin
        k:=SDLK_RIGHT;
        if y<TopY then
            k:=SDLK_KP9;
        if y>BottomY then
            k:=SDLK_KP3;
    end;

    if (x>LeftX-1) and (x<RightX) then
    begin
//         if (y>239) and (y<261) then
//             k:=ord(KeyInventory);
        if y<TopY then
            k:=SDLK_UP;
        if y>BottomY then
            k:=SDLK_DOWN;
    end;

    // icons
    if (x>=13) and (x<=310) and (y>=445) and (y<=492) then
    begin
        if (x>=13) and (x<=60) then
            k:=ord(KeyStatus);

        if (x>=63) and (x<=110) then
            k:=ord(KeyInventory);

        if (x>=113) and (x<=160) then
            k:=ord(KeyChant);

        if (x>=163) and (x<=210) then
            k:=ord(KeyTrade);

        if (x>=213) and (x<=260) then
            if ThePlayer.intWeapon>0 then
                if Thing[ThePlayer.intWeapon].chLetter=chr(158) then
                    k:=ord(KeyShoot);

        if (x>=263) and (x<=310) and (LastSong>0) then
            k:=ord(KeyChantLast);
    end
    else
        KeyRepeatSoll:=GetKeyRepeat(x, y, k);

    TransformClickInKey:=k;
end;


function cookKey(unicode, sdlkey: integer): integer;
var
  modifier:TSDLMod;
begin
  modifier := SDL_GetModState();
  //Check is (any) CTRL pressed.
  //This is for keyboards without F1-F12.
    if KMOD_CTRL and Modifier>0 then
    begin
      //Writeln('CTRL pressed');
      case unicode of
        17: cookKey:=SDLK_F1;    //q
        23: cookKey:=SDLK_F2;    //w
         5: cookKey:=SDLK_F3;    //e
        18: cookKey:=SDLK_F4;    //r
        20: cookKey:=SDLK_F5;    //t
        25: cookKey:=SDLK_F6;    //y
        21: cookKey:=SDLK_F7;    //u
         9: cookKey:=SDLK_F8;    //i
        15: cookKey:=SDLK_F9;    //o
        16: cookKey:=SDLK_F10;   //p
        44: cookKey:=SDLK_F11;   //,
        46: cookKey:=SDLK_F12;   //.
      end;
    end

    else if (unicode>31) and (unicode<128) then
        cookKey:=unicode
    else
    begin
        case sdlkey of
            SDLK_KP1:
                cookKey:=SDLK_KP1;
            SDLK_KP2:
                cookKey:=SDLK_KP2;
            SDLK_KP3:
                cookKey:=SDLK_KP3;
            SDLK_KP4:
                cookKey:=SDLK_KP4;
            SDLK_KP6:
                cookKey:=SDLK_KP6;
            SDLK_KP7:
                cookKey:=SDLK_KP7;
            SDLK_KP8:
                cookKey:=SDLK_KP8;
            SDLK_KP9:
                cookKey:=SDLK_KP9;
            SDLK_UP:
                cookKey:=SDLK_UP;
            SDLK_DOWN:
                cookKey:=SDLK_DOWN;
            SDLK_PAGEUP:
                cookKey:=SDLK_PAGEUP;
            SDLK_PAGEDOWN:
                cookKey:=SDLK_PAGEDOWN;
            SDLK_LEFT:
                cookKey:=SDLK_LEFT;
            SDLK_RIGHT:
                cookKey:=SDLK_RIGHT;
            SDLK_KP0:
                cookKey:=SDLK_INSERT;
            13:
                cookKey:=13;
            SDLK_KP_ENTER:
                cookKey:=13;
            8:  //Backspace
               cookKey:=SDLK_ESCAPE;
            SDLK_ESCAPE:
                cookKey:=SDLK_ESCAPE;
            SDLK_F1:
                cookKey:=SDLK_F1;
            SDLK_F2:
                cookKey:=SDLK_F2;
            SDLK_F3:
                cookKey:=SDLK_F3;
            SDLK_F4:
                cookKey:=SDLK_F4;
            SDLK_F5:
                cookKey:=SDLK_F5;
            SDLK_F6:
                cookKey:=SDLK_F6;
            SDLK_F7:
                cookKey:=SDLK_F7;
            SDLK_F8:
                cookKey:=SDLK_F8;
            SDLK_F9:
                cookKey:=SDLK_F9;
            SDLK_F10:
                cookKey:=SDLK_F10;
            SDLK_F11:
                cookKey:=SDLK_F11;
            SDLK_F12:
                cookKey:=SDLK_F12;
        end;
    end;
end;


function ConsoleInput(): integer;
var
    k: TKeyEvent;
    zeichen: char;
    kette: string;
begin
    ConsoleInput:=0;

    k:=GetKeyEvent; //Get the next raw key event, wait if needed.
    k:=TranslateKeyEvent(k); //Translate raw event to ascii key event

    if IsFunctionKey(k)=true then
    begin
        kette:=KeyEventToString(k);  //Return a string describing the key event.
        if kette='Left' then
            ConsoleInput:=SDLK_LEFT
        else
        if kette='Right' then
            ConsoleInput:=SDLK_RIGHT
        else
        if kette='Up' then
            ConsoleInput:=SDLK_UP
        else
        if kette='Down' then
            ConsoleInput:=SDLK_DOWN
        else
        if kette='Home' then
            ConsoleInput:=SDLK_KP7
        else
        if kette='PgUp' then
            ConsoleInput:=SDLK_PAGEUP
        else
        if kette='End' then
            ConsoleInput:=SDLK_KP1
        else
        if kette='PgDn' then
            ConsoleInput:=SDLK_PAGEDOWN
        else
        if kette='Delete' then
            ConsoleInput:=SDLK_DELETE
        else
        if kette='Insert' then
            ConsoleInput:=SDLK_INSERT;
        if kette='F1' then
            ConsoleInput:=SDLK_F1;
        if kette='F2' then
            ConsoleInput:=SDLK_F2;
        if kette='F3' then
            ConsoleInput:=SDLK_F3;
        if kette='F4' then
            ConsoleInput:=SDLK_F4;
        if kette='F5' then
            ConsoleInput:=SDLK_F5;
        if kette='F6' then
            ConsoleInput:=SDLK_F6;
        if kette='F7' then
            ConsoleInput:=SDLK_F7;
        if kette='F8' then
            ConsoleInput:=SDLK_F8;
        if kette='F9' then
            ConsoleInput:=SDLK_F9;
        if kette='F10' then
            ConsoleInput:=SDLK_F10;
        if kette='F11' then
            ConsoleInput:=SDLK_F11;
        if kette='F12' then
            ConsoleInput:=SDLK_F12;

        // process keys that are not covered by KeyEventToString
        if GetKeyEventCode(K)=$011B then
            ConsoleInput:=SDLK_ESCAPE;
    end
    else
    begin
//         kette:=KeyEventToString(k);
//         zeichen:=kette[1];

        zeichen:=GetKeyEventChar(k);   //Get the character key part of a key event.
        ConsoleInput:=ord(zeichen);
    end;

//     if (ord(zeichen)>64) and (ord(zeichen)<91) then
//         ConsoleInput:=0;

end;



function GetDirection(mode: integer): char;
var
    chKey: char;
    n: integer;
    kx, ky, knoepfe: longint;
begin

    BottomBar;

    if mode=0 then
        ShowMessage('Direction?  [press the according direction key]', false)
    else
    if mode=1 then
//         ShowMessage('[right] and [left] to select, [down] to select, [up] to cancel');
        mode:=mode;

    chKey:=' ';

    repeat
        if UseSDL=true then
        begin
            if SDL_WaitEvent( @MyEvent ) > 0 then
            begin
                KX:=MyEvent.motion.x;
                KY:=MyEvent.motion.y;
                Knoepfe := SDL_GetMouseState(KX, KY);
                case MyEvent.type_ of
                    SDL_KEYDOWN:
                        n:=cookKey(MyEvent.key.keysym.unicode, MyEvent.key.keysym.sym);
                end;
                if ((Knoepfe and SDL_BUTTON(SDL_BUTTON_LEFT))<>0) and (UseMouseToMove=true) then
                    n:=TransformClickInKey(kx, ky);
            end;
        end
        else
            n:=ConsoleInput();

        // evaluate key
        if n=ord(KeyNorth) then
            chKey:=KeyNorth
        else
        if n=ord(KeySouth) then
            chKey:=KeySouth
        else
        if n=ord(KeyEast) then
            chKey:=KeyEast
        else
        if n=ord(KeyWest) then
            chKey:=KeyWest
        else
        if n=ord(KeyNorthEast) then
            chKey:=KeyNorthEast
        else
        if n=ord(KeyNorthWest) then
            chKey:=KeyNorthWest
        else
        if n=ord(KeySouthEast) then
            chKey:=KeySouthEast
        else
        if n=ord(KeySouthWest) then
            chKey:=KeySouthWest;

        case n of
            SDLK_UP:
                chKey := KeyNorth;
            SDLK_KP8:
                chKey := KeyNorth;
            SDLK_DOWN:
                chKey := KeySouth;
            SDLK_KP2:
                chKey := KeySouth;
            SDLK_LEFT:
                chKey := KeyWest;
            SDLK_KP4:
                chKey := KeyWest;
            SDLK_RIGHT:
                chKey := KeyEast;
            SDLK_KP6:
                chKey := KeyEast;
            SDLK_KP9:
                chKey := KeyNorthEast;
            SDLK_KP7:
                chKey := KeyNorthWest;
            SDLK_KP3:
                chKey := KeySouthEast;
            SDLK_KP1:
                chKey := KeySouthWest;
            ord('1'):
                chKey := KeySouthWest;
            ord('2'):
                chKey := KeySouth;
            ord('3'):
                chKey := KeySouthEast;
            ord('4'):
                chKey := KeyWest;
            ord('6'):
                chKey := KeyEast;
            ord('7'):
                chKey := KeyNorthWest;
            ord('8'):
                chKey := KeyNorth;
            ord('9'):
                chKey := KeyNorthEast;
        end;


    until (chKey=KeyNorth) or (chKey=KeySouth) or (chKey=KeyEast) or (chKey=KeyWest) or (chKey=KeyNorthEast) or (chKey=KeySouthEast) or (chKey=KeyNorthWest) or (chKey=KeySouthWest);

//    ShowMessage('Direction? ' + chKey);
    Delay(100);

    GetDirection := chKey;
end;


function GetRepeat(mode: integer): integer;
var
    k, n: integer;
    strInput: string;
    blOK: boolean;
    message: string;
    chNumPad: string;
begin
    if mode=0 then
        message:='Repeat?'
    else
    if mode=2 then
        message:='Amount?'
    else
    if mode=3 then
        message:='Enter correct passcode: '
    else
        message:='Selection?';

    ShowMessage(message + ' _', false);

    strInput := '';
    blOK := false;

    repeat
        chNumPad:='';

        if UseSDL=true then
        begin
            if SDL_WaitEvent( @MyEvent ) > 0 then
                case MyEvent.type_ of
                    SDL_KEYDOWN:
                        k:=cookKey(MyEvent.key.keysym.unicode, MyEvent.key.keysym.sym);
                end
        end
        else
            k:=ConsoleInput();

        case k of
            13:
                blOK := true;

//             8:        begin
//                         strInput := '';
//                         ShowMessage(message + '                                                                      ', false);
//                     end;

            8:
            begin
                strInput:=copy(strInput, 1, length(strInput)-1);
                ShowMessage(message + ' ' + strInput  + '_', false);
            end;


            SDLK_LEFT:
            begin
                strInput:=copy(strInput, 1, length(strInput)-1);
                ShowMessage(message + ' ' + strInput  + '_', false);
            end;

            SDLK_KP1:
                chNumPad:='1';
            SDLK_KP2:
                chNumPad:='2';
            SDLK_KP3:
                chNumPad:='3';
            SDLK_KP4:
                chNumPad:='4';
            SDLK_KP5:
                chNumPad:='5';
            SDLK_KP6:
                chNumPad:='6';
            SDLK_KP7:
                chNumPad:='7';
            SDLK_KP8:
                chNumPad:='8';
            SDLK_KP9:
                chNumPad:='9';
        end;
    
        if chNumPad<>'' then
            if length(strInput) < 4 then
            begin
                strInput := strInput + chNumPad;
                ShowMessage(message + ' ' + strInput  + '_', false);
            end;

        if (k > 47) and (k < 58) then
            if length(strInput) < 4 then
            begin
                strInput := strInput + chr(k);
                ShowMessage(message + ' ' + strInput + '_', false);
            end;


    until (blOK = true);

    Val(strInput, n);

    GetRepeat := n;
end;

//Meant for call only inside this file
function internal_GetInput(message: string; MaxLength: integer; DigitsOnly: boolean): string;
var
    k: integer;
    strInput: string;
    blOK: boolean;
    min_ascii:integer; 
    max_ascii:integer;
begin

    if DigitsOnly then
    begin
      min_ascii := 48;
      max_ascii := 57;
    end
    else
    begin
      min_ascii := 32;
      max_ascii := 128;
    end;
      
    BottomBar;
    ShowMessage(message + ' _', false);

    strInput := '';
    blOK := false;

    repeat

        k:=0;

        if UseSDL=true then
        begin
            if SDL_WaitEvent( @MyEvent ) > 0 then
                case MyEvent.type_ of
                    SDL_KEYDOWN:
                        k:=cookKey(MyEvent.key.keysym.unicode, MyEvent.key.keysym.sym);
                end
        end
        else
            k:=ConsoleInput();

        case k of
            13: //Enter
                blOK := true;

            8:  //Backspace
            begin
                strInput:=copy(strInput, 1, length(strInput)-1);
                ShowMessage(message + ' ' + strInput  + '_', false);
            end;

            SDLK_LEFT:
            begin
                strInput:=copy(strInput, 1, length(strInput)-1);
                ShowMessage(message + ' ' + strInput  + '_', false);
            end;

            //Threat keypad numbers as ordinal numbers
            SDLK_KP0:
                k := 48;
            SDLK_KP1:
                k := 49;
            SDLK_KP2:
                k := 50;
            SDLK_KP3:
                k := 51;
            SDLK_KP4:
                k := 52;
            SDLK_KP5:
                k := 53;
            SDLK_KP6:
                k := 54;
            SDLK_KP7:
                k := 55;
            SDLK_KP8:
                k := 56;
            SDLK_KP9:
                k := 57;

        //N900: q is same key than 1 (etc) 
        // so it is handy to accept 'q' as '1' when entering digits
        113: if DigitsOnly then k:=49;    //q
        119: if DigitsOnly then k:=50;    //w
        101: if DigitsOnly then k:=51;    //e
        114: if DigitsOnly then k:=52;    //r
        116: if DigitsOnly then k:=53;    //t
        121: if DigitsOnly then k:=54;    //y
        117: if DigitsOnly then k:=55;    //u
        105: if DigitsOnly then k:=56;    //i
        111: if DigitsOnly then k:=57;    //o
        112: if DigitsOnly then k:=48;    //p
        end;

        if (k >= min_ascii) and (k <= max_ascii) then
        begin
            if length(strInput) < MaxLength then
            begin
                strInput := strInput + chr(k);
                ShowMessage(message + ' ' + strInput  + '_', false);
            end;
        end;


    until (blOK = true);

    internal_GetInput := strInput;
    LastMessage:='-';
end;


function GetTextInput(message: string; MaxLength: integer): string;
begin
    GetTextInput := internal_GetInput(message,MaxLength,false);
end;

//Returns integer. Does accept only digits.
function GetNumericalInput(message: string; MaxLength: integer): integer;
var
    return_value: integer;
    i: integer;
    temp: integer;
    potence: integer;
    strInput: string;
begin

    //accept only digits. 0...9
    strInput := internal_GetInput(message,MaxLength,true);


    return_value := 0;
    potence := 1;
    for i := length(strInput) downto 1 do //loops from the last character to the first
    begin
      Val (strInput[i], temp);
      temp := potence*temp;
      //Writeln(inttostr(temp));
      return_value := return_value + temp;
      potence := potence * 10;
    end;

    GetNumericalInput := return_value;
    LastMessage:='-';
end;


function GetKeyInput(message: string; pressanykey: boolean): string;
var
    k: integer;
    kx, ky, knoepfe: longint;
    strInput: string;
    blOK: boolean;
    chNumPad: string;
begin

    if (message<>' ') or (pressanykey=true) then
    begin
        if pressanykey=false then
        begin
            TransTextXY(1,1+PlaceOfBottomBar,message);
            if UseSDL=true then
                SDL_UPDATERECT(screen,0,0,0,0)
            else
                UpdateScreen(true);
        end
        else
        begin
            BottomBar;
            ShowMessage(message, true);
        end;
    end
    else
    begin
        // explicitly update screen if not updated by ShowMessage
        if UseSDL=true then
            SDL_UPDATERECT(screen,0,0,0,0)
        else
            UpdateScreen(true);
    end;

    strInput := '';
    blOK := false;

    repeat
        chNumPad:='';


        // when we set k to -1, it seems that all messages stay where they should ...
        k:=-1;

        if UseSDL=true then
        begin

            if SDL_WaitEvent( @MyEvent ) > 0 then
            begin
                if UseMouseToMove=true then
                begin
                    kx:=MyEvent.motion.x;
                    ky:=MyEvent.motion.y;
                    knoepfe := SDL_GetMouseState(kx, ky);
                    if (Knoepfe and SDL_BUTTON(SDL_BUTTON_RIGHT))<>0 then
                        k:=13;
                end;
                if MyEvent.type_ = SDL_KEYDOWN then
                    k:=cookKey(MyEvent.key.keysym.unicode, MyEvent.key.keysym.sym);
            end;
        end
        else
            k:=ConsoleInput();

        case k of
            13:
            begin
                strInput := 'ENTER';
                blOK:=true;
            end;
            32:
                if pressanykey=true then
                    blOK:=true;
            8:
            begin
                strInput := '';
                //if message<>' ' then
                //begin
                  //  ShowMessage('                                                                            ', false);
                    //ShowMessage(message, false);
                //end;
            end;

            SDLK_KP1:
                chNumPad:='1';
            SDLK_KP2:
                chNumPad:='2';
            SDLK_KP3:
                chNumPad:='3';
            SDLK_KP4:
                chNumPad:='4';
            SDLK_KP5:
                chNumPad:='5';
            SDLK_KP6:
                chNumPad:='6';
            SDLK_KP7:
                chNumPad:='7';
            SDLK_KP8:
                chNumPad:='8';
            SDLK_KP9:
                chNumPad:='9';

            SDLK_LEFT:
                chNumPad:=' ';
            SDLK_RIGHT:
                chNumPad:=' ';
            SDLK_UP:
                chNumPad:=' ';
            SDLK_DOWN:
                chNumPad:=' ';

            SDLK_PAGEUP:
            begin
                strInput := '-';
                blOK:=true;
            end;

            SDLK_PAGEDOWN:
            begin
                strInput := '+';
                blOK:=true;
            end;


            SDLK_ESCAPE:
            begin
                strInput := 'ESC';  // this ESC is totally arbitray
                blOK:=true;
            end;

            SDLK_F1:
            begin
                strInput := 'F1';
                blOK:=true;
            end;

            SDLK_F2:
            begin
                strInput := 'F2';
                blOK:=true;
            end;

            SDLK_F3:
            begin
                strInput := 'F3';
                blOK:=true;
            end;

            SDLK_F4:
            begin
                strInput := 'F4';
                blOK:=true;
            end;

            SDLK_F5:
            begin
                strInput := 'F5';
                blOK:=true;
            end;

            SDLK_F6:
            begin
                strInput := 'F6';
                blOK:=true;
            end;

            SDLK_F7:
            begin
                strInput := 'F7';
                blOK:=true;
            end;

            SDLK_F8:
            begin
                strInput := 'F8';
                blOK:=true;
            end;

            SDLK_F9:
            begin
                strInput := 'F9';
                blOK:=true;
            end;

            SDLK_F10:
            begin
                strInput := 'F10';
                blOK:=true;
            end;

            SDLK_F11:
            begin
                strInput := 'F11';
                blOK:=true;
            end;

            SDLK_F12:
            begin
                strInput := 'F12';
                blOK:=true;
            end;


        end;

        if chNumPad<>'' then
            if length(strInput) < 1 then
            begin
                strInput := strInput + chNumPad;
                //if message<>' ' then
                    //ShowMessage(message, false);
                blOK:=true;
            end;

        if (k > 31) and (k < 127) then
            if length(strInput) < 1 then
            begin
                strInput := strInput + chr(k);
                //if message<>' ' then
                    //ShowMessage(message, false);
                blOK:=true;
            end;

        if (pressanykey=true) and (k<>13) and (k<>32) then
            blOK:=false;


    until (blOK = true);

    GetKeyInput := strInput;
    LastMessage:='-';
end;


end.
