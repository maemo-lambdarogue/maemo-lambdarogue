{
     Copyright (C) 2006-2008 by Mario Donick
     mario.donick@gmail.com

     This program is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation; either version 2 of the License, or
     (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program; if not, write to the
     Free Software Foundation, Inc.,
     59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
}

unit CollectData;

interface

uses
    Classes, SysUtils, Player, Items;


function ReturnStatusIncRate(intE: integer): integer;
function MaximizedWeaponSkill: boolean;
function CollectTP: integer;
function CollectGunTP: integer;
function CollectDP: integer;
function CollectBuyPrice(n: integer): integer;
function CollectSellPrice(n: Integer): Integer;

implementation

// returns the bonus value of an equipped item
function ReturnStatusIncRate(intE: integer): integer;
var
    t: integer;
begin
    t := 0;
    if ThePlayer.intWeapon>0 then
        if Thing[ThePlayer.intWeapon].intEffect=intE then
            inc(t, Thing[ThePlayer.intWeapon].intRange);

    if ThePlayer.intArmour>0 then
        if Thing[ThePlayer.intArmour].intEffect=intE then
            inc(t, Thing[ThePlayer.intArmour].intRange);

    if ThePlayer.intHat>0 then
        if Thing[ThePlayer.intHat].intEffect=intE then
            inc(t, Thing[ThePlayer.intHat].intRange);

    if ThePlayer.intRingLeft>0 then
        if Thing[ThePlayer.intRingLeft].intEffect=intE then
            inc(t, Thing[ThePlayer.intRingLeft].intRange);

    if ThePlayer.intRingRight>0 then
        if Thing[ThePlayer.intRingRight].intEffect=intE then
            inc(t, Thing[ThePlayer.intRingRight].intRange);

    if ThePlayer.intFeet>0 then
        if Thing[ThePlayer.intFeet].intEffect=intE then
            inc(t, Thing[ThePlayer.intFeet].intRange);

    if ThePlayer.intExtra>0 then
        if Thing[ThePlayer.intExtra].intEffect=intE then
            inc(t, Thing[ThePlayer.intExtra].intRange);

    ReturnStatusIncRate := t;
end;


// calculate total DP
function CollectDP: integer;
var
    intDP: integer;
begin
    intDP := ThePlayer.intFight div 4;

    if ThePlayer.intArmour>0 then
        inc(intDP, Thing[ThePlayer.intArmour].intAP);

    if ThePlayer.intWeapon>0 then
        inc(intDP, Thing[ThePlayer.intWeapon].intAP);

    if ThePlayer.intHat>0 then
        inc(intDP, Thing[ThePlayer.intHat].intAP);

    if ThePlayer.intRingLeft>0 then
        inc(intDP, Thing[ThePlayer.intRingLeft].intAP);

    if ThePlayer.intRingRight>0 then
        inc(intDP, Thing[ThePlayer.intRingRight].intAP);

    if ThePlayer.intFeet>0 then
        inc(intDP, Thing[ThePlayer.intFeet].intAP);

    if ThePlayer.intExtra>0 then
        inc(intDP, Thing[ThePlayer.intExtra].intAP);

    if ThePlayer.blBlessed=true then
        inc(intDP);

    // itemset 2 ("redemption") gives 10% extra DP
    if RuneSet('Cape of Magic', 'Barbute of Invisibility', 'Ring of Wisdom', 'Ring of Agility', 'Ring of Agility', false) = true then
        inc(intDP, trunc((20*intDP) div 100));

    CollectDP:=intDP;
end;

// returns true if the player's weapon skill is the same or higher than the
// WP (or GP in case of a firearm) of his current weapon
function MaximizedWeaponSkill: boolean;
var
    blTmp: boolean;
begin
    blTmp := true;

    if ThePlayer.intWeapon > 0 then
    begin
        if Thing[ThePlayer.intWeapon].chLetter=chr(156) then
            if ThePlayer.intSword < Thing[ThePlayer.intWeapon].intWP then
                blTmp := false;

        if Thing[ThePlayer.intWeapon].chLetter=chr(159) then
            if ThePlayer.intAxe < Thing[ThePlayer.intWeapon].intWP then
                blTmp := false;

        if Thing[ThePlayer.intWeapon].chLetter=chr(164) then
            if ThePlayer.intWhip < Thing[ThePlayer.intWeapon].intWP then
                blTmp := false;

        if Thing[ThePlayer.intWeapon].chLetter=chr(158) then
            if ThePlayer.intGun < Thing[ThePlayer.intWeapon].intGP then
                blTmp := false;
    end
    else
        blTmp := false;

    MaximizedWeaponSkill := blTmp;
end;

// calculate total TP (shown as "M" in "MLD")
function CollectTP: integer;
var
    intTP: integer;
begin
    intTP := ThePlayer.intFight;

    // add bonuses of equipped non-weapon items
    if ThePlayer.intArmour>0 then
        inc(intTP, Thing[ThePlayer.intArmour].intWP);

    if ThePlayer.intHat>0 then
        inc(intTP, Thing[ThePlayer.intHat].intWP);

    if ThePlayer.intRingLeft>0 then
        inc(intTP, Thing[ThePlayer.intRingLeft].intWP);

    if ThePlayer.intRingRight>0 then
        inc(intTP, Thing[ThePlayer.intRingRight].intWP);

    if ThePlayer.intFeet>0 then
        inc(intTP, Thing[ThePlayer.intFeet].intWP);

    if ThePlayer.intExtra>0 then
        inc(intTP, Thing[ThePlayer.intExtra].intWP);

    // add weapon WP
    if ThePlayer.intWeapon>0 then
    begin

        inc(intTP, Thing[ThePlayer.intWeapon].intWP);

        // if weapon-skill is lower than weapon's WP,
        // decrease TP by this difference

        if Thing[ThePlayer.intWeapon].chLetter=chr(156) then
            if ThePlayer.intSword < Thing[ThePlayer.intWeapon].intWP then
                dec(intTP, Thing[ThePlayer.intWeapon].intWP - ThePlayer.intSword);

        if Thing[ThePlayer.intWeapon].chLetter=chr(159) then
            if ThePlayer.intAxe < Thing[ThePlayer.intWeapon].intWP then
                dec(intTP, Thing[ThePlayer.intWeapon].intWP - ThePlayer.intAxe);

        if Thing[ThePlayer.intWeapon].chLetter=chr(164) then
            if ThePlayer.intWhip < Thing[ThePlayer.intWeapon].intWP then
                dec(intTP, Thing[ThePlayer.intWeapon].intWP - ThePlayer.intWhip);

        if ThePlayer.intStrength>100 then
            inc(intTP, ThePlayer.intStrength-100);
    end;

    // itemset 5 ("Minor Gear of Thagor") gives 20% extra TP
    if RuneSet('Fire Blade of Thagor', 'Great Helm of Thagor', 'Great Helm of Thagor', 'Great Helm of Thagor', 'Great Helm of Thagor', false) = true then
        inc(intTP, trunc((20*intTP) div 100));

    CollectTP:=intTP;
end;


// calculate total TP of a long-range weapon (shown as "L" in "MLD")
function CollectGunTP: integer;
var
    intTP: integer;
begin
    intTP:=ThePlayer.intFight;

    // add weapon GP
    if ThePlayer.intWeapon>0 then
    begin
        inc(intTP, Thing[ThePlayer.intWeapon].intGP);

        // if gun-skill is lower than weapon's GP,
        // decrease TP by this difference
        if ThePlayer.intGun < Thing[ThePlayer.intWeapon].intGP then
            dec(intTP, Thing[ThePlayer.intWeapon].intGP - ThePlayer.intGun);
    end;

    // add bonuses of equipped non-weapon items
    if ThePlayer.intArmour>0 then
        inc(intTP, Thing[ThePlayer.intArmour].intGP);

    if ThePlayer.intHat>0 then
        inc(intTP, Thing[ThePlayer.intHat].intGP);

    if ThePlayer.intRingLeft>0 then
        inc(intTP, Thing[ThePlayer.intRingLeft].intGP);

    if ThePlayer.intRingRight>0 then
        inc(intTP, Thing[ThePlayer.intRingRight].intGP);

    if ThePlayer.intFeet>0 then
        inc(intTP, Thing[ThePlayer.intFeet].intGP);

    if ThePlayer.intExtra>0 then
        inc(intTP, Thing[ThePlayer.intExtra].intGP);

    // itemset 6 ("Major Gear of Thagor") gives 20% extra GunTP
    if RuneSet('Precise Bow of Thagor', 'Precious Ring of Thagor', 'Precious Ring of Thagor', 'Precious Ring of Thagor', 'Precious Ring of Thagor', false) = true then
        inc(intTP, trunc((20*intTP) div 100));

    CollectGunTP := intTP;
end;


// collect total buy price
function CollectBuyPrice(n: integer): integer;
var
    p: integer;
begin
    p := thing[n].intPrice;

    if ThePlayer.intTrade>3 then
        dec(p,(1*p) div 100);
    if ThePlayer.intTrade>6 then
        dec(p,(3*p) div 100);
    if ThePlayer.intTrade>9 then
        dec(p,(6*p) div 100);
    if ThePlayer.intTrade>11 then
        dec(p,(8*p) div 100);

    if PlayerHasItem('V.I.P. Card')>0 then
        p:=p div 2;

    if p<1 then
        p:=1;

    CollectBuyPrice := p;
end;


// collect sell price
function CollectSellPrice(n: Integer): Integer;
var
    bp, tp: Integer;
begin
    bp:=Thing[n].intPrice;
    tp:=(8*bp) div 100;
    inc(tp, ((5*ThePlayer.intTrade)*bp) div 100);
    if tp>Thing[n].intPrice then
        tp:=Thing[n].intPrice;
    CollectSellPrice:=tp;
end;

end.
