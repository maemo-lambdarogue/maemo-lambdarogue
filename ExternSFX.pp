{
     Copyright (C) 2006-2008 by Mario Donick
     mario.donick@gmail.com

     This program is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation; either version 2 of the License, or
     (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program; if not, write to the
     Free Software Foundation, Inc.,
     59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
}

unit ExternSFX;


interface

uses
    ExternMusic, Process, crt, RandomArea;

function SFXPlaying: Boolean;
procedure PlaySFX(SoundFile: string);
procedure StopSFX;
procedure InitSFX;
procedure FreeSFX;


var
    ExtProgramTwo: TProcess;
    blUseExternPlayerTwo: Boolean;

implementation


function SFXPlaying: Boolean;
begin
    if ExtProgramTwo<>nil then
        SFXPlaying := ExtProgramTwo.Running;
end;


procedure StopSFX;
begin
    if ExtProgramTwo.Running=true then
        ExtProgramTwo.Terminate(0);
end;


procedure PlaySFX(SoundFile: string);
begin
    StopSFX;
    if blUseExternPlayerTwo=true then
    begin
        ExtProgramTwo.CommandLine :=strExternPlayer+' '+PathData+'sound/'+Soundfile;
        //writeln('Executing '+ExtProgramTwo.CommandLine);
        ExtProgramTwo.Options := ExtProgramTwo.Options + [poUsePipes];
        ExtProgramTwo.Execute;
    end;
end;


procedure InitSFX;
begin
    ExtProgramTwo := TProcess.Create(nil);
end;


procedure FreeSFX;
begin
    ExtProgramTwo.free;
end;

end.
