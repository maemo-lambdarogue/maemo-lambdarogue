@echo off
echo.
echo LambdaRogue 1.5
echo.

:parameters
if "%1"=="" goto start
if "%1"=="-help" goto help
if "%1"=="-del-bones" goto delbones
if "%1"=="-import-bones" goto importbones
if "%1"=="-merge-bones" goto mergebones
if "%1"=="-export-bones" goto exportbones
if "%1"=="-show-dump" goto showdump
if "%1"=="-export-dump" goto exportdump
goto help
goto end

:start
echo Starting the game (try startlr -help for options)
lambdarogue
goto end

:help
echo Usage: startlr [OPTION] [CHARACTER NAME] [FILE]
echo.
echo  -del-bones        delete bone file
echo  -import-bones     import file as new bone file
echo  -merge-bones      merge bone file with specified file
echo  -export-bones     export current bone file to specified file
echo  -show-dump        shows specified character dump
echo  -export-dump      exports character dump to specified file
echo  -help             show this help
echo.
echo Executing without parameter starts the game.
echo.
echo Examples:
echo.
echo   startlr                               starts LambdaRogue
echo   startlr -del-bones                    delete current bone file
echo   startlr -import-bones anne.txt        use anne.txt as current bone file
echo   startlr -merge-bones peter.txt        merge current bone file with peter.txt
echo   startlr -export-bones anne.txt        export current bone file to anne.txt
echo   startlr -show-dump Tharyn             outputs character dump of Tharyn
echo   startlr -export-dump Odin peter.txt   export chardump of Odin to peter.txt
echo.
echo New versions, patches and bug fixes: http://donick.net/lr
goto end

:delbones
if not exist data\bones.txt goto delbones_error
del data\bones.txt
echo Bone file deleted.
goto end

:delbones_error
echo Error: No bone file available.
goto end

:exportbones
if not exist data\bones.txt goto exportbones_error1
copy /y data\bones.txt %2 > nul
echo Bone file exported to %2.
goto end

:exportbones_error1
echo Error: No bone file available.
goto end

:mergebones
if not exist %2 goto mergebones_error1
if not exist data\bones.txt goto mergebones_error2
if exist bones.tmp del bones.tmp
copy /b data\bones.txt+%2 bones.tmp > nul
del data\bones.txt
move bones.tmp data\bones.txt
echo Bone file merged with %2.
goto end

:mergebones_error1
echo Could not merge bone file. %2 not found.
goto end

:mergebones_error2
echo Error: No bone file available.
goto end

:importbones
if not exist %2 goto importbones_error1
if exist data\bones.txt goto importbones_error2
copy %2 data\bones.txt > nul
echo Bone file %2 imported.
goto end

:importbones_error1
echo Error: Could not create bone file. %2 not found.
goto end

:importbones_error2
echo Error: Bone file already exists (try startlr -merge instead).
goto end

:showdump
if not exist saves\%2-dump.txt goto showdump_error
type saves\%2-dump.txt | more
goto end

:showdump_error
echo Error: No dump for a character named %2 available.
goto end

:exportdump
if not exist saves\%2-dump.txt goto exportdump_error1
copy /y saves\%2-dump.txt %3 > nul
echo Character dump of %2 exported to %3.
goto end

:exportdump_error1
echo Error: No dump for a character named %2 available.
goto end

:end
