-- LambdaRogue: The Book of Stars -- v1.5 --


INSTALLATION INSTRUCTIONS:

If you've downloaded an installer (Windows only), just execute it. If you
do not have administrator permissions on your system, you have to change
the install path to somewhere in "My Documents".

If you've downloaded an archive (zip for the Windows-version or tar/bz2 for
the Linux-version), extract the archive.


HOW TO GET MUSIC (IF YOU'VE DOWNLOADED A PACKAGE WITHOUT MUSIC FILES)

If you want to hear music during the game, you have to download the
LambdaRogue music package and extract the mp3-files in the folder "music"
within the directory where you've installed or extracted the game itself.

You have to activate music playback in the options menu before you will
hear it!


STARTING THE GAME:

If you have used the installer, just click the LambdaRogue entry in the start
menu. Otherwise, run startlr.bat (on Windows) or startlr (on Linux) to start
the game.

Note that startlr.bat and startlr provide some options which you can access
from the commandline or a terminal. Enter "startlr -help" (Windows) or
"startlr --help" (Linux) to see valid options.
